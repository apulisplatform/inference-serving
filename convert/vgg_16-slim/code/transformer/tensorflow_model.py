import base64
import logging
from typing import Dict

import cv2
import kfserving
import numpy as np
import tritonclient.http as httpclient

from .frame.model_parser import ModelParser


class TensorFlowModel(kfserving.KFModel):
    def __init__(self, name: str, predictor_host: str):
        super().__init__(name)
        self.load()
        self.triton_client = None
        self.model_parser = None
        self.predictor_host = predictor_host

    def to_numpy(self, tensor):
        return tensor.detach().cpu().numpy() if tensor.requires_grad else tensor.cpu().numpy()

    def preprocess(self, inputs: Dict) -> Dict:
        # 会自动修改inputs
        model_parser = self.get_model_metadata()
        for input in inputs["inputs"]:
            images_data = input["data"]
            images_data = base64.b64decode(images_data)
            images_data = np.asarray(bytearray(images_data), dtype="uint8")
            image = cv2.imdecode(images_data, cv2.IMREAD_COLOR)
            width = None
            height = None
            dtype = model_parser["inputs"][0]["datatype"]
            for shape in model_parser["inputs"][0]["shape"]:
                if shape != -1 and shape != 1 and shape != 3:
                    if not width:
                        width = shape
                    else:
                        height = shape
            image = cv2.resize(image, (width, height))
            logging.error(image.shape)
            logging.error(dtype)
            if "FLOAT" in dtype:
                image = np.asarray(image).astype(np.float32)
                # image = np.transpose(image, [2, 0, 1])
                image = image / 255
            image = np.expand_dims(image, 0)
            input["data"] = image
        return inputs

    def predict(self, inputs: Dict) -> Dict:
        request_id = ""
        model_parser = self.get_model_metadata()
        logging.error(model_parser)
        new_inputs = []
        for idx, input in enumerate(inputs["inputs"]):
            input_data = input["data"]
            node = model_parser["inputs"][idx]
            name, shape, dtype = node["name"], node["shape"], node["datatype"]
            logging.error(name, shape, dtype)
            new_inputs.append(httpclient.InferInput(name,  input_data.shape, dtype))
            new_inputs[idx].set_data_from_numpy(input_data)
        outputs = []
        for idx in range(len(model_parser["outputs"])):
            node = model_parser["outputs"][idx]
            output = httpclient.InferRequestedOutput(node["name"])
            outputs.append(output)
        logging.error("begin infer")
        logging.error(inputs)
        logging.error(model_parser["name"])

        result = self.triton_client.infer(model_parser["name"], new_inputs, outputs=outputs, request_id=request_id)
        logging.error("after infer")
        return result

    def postprocess(self, result: Dict) -> Dict:
        postprocess_result = result.get_response()
        model_parser = self.get_model_metadata()
        outputs = []
        for idx in range(len(model_parser["outputs"])):
            node = model_parser["outputs"][idx]
            logging.error(node)
            res_data = result.as_numpy(node["name"])
            outputs.append({'name': node["name"], 'datatype': str(res_data.dtype),
                            'shape': list(res_data.shape),
                            "data": res_data.tolist()})
        postprocess_result['outputs'] = outputs
        return postprocess_result

    def init_client(self):
        if not self.triton_client:
            self.triton_client = httpclient.InferenceServerClient(
                url=self.predictor_host, verbose=False)

    def get_model_parser(self):
        if self.model_parser == None:
            model_metadata = self.get_model_metadata()
            model_config = self.get_config()
            self.model_parser = ModelParser(model_metadata, model_config)
        return self.model_parser

    def get_config(self):
        self.init_client()
        return self.triton_client.get_model_config(model_name=self.name)

    def get_model_metadata(self):
        self.init_client()
        return self.triton_client.get_model_metadata(model_name=self.name)
