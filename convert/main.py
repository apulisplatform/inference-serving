############################################################################
# 2019 - present Contributed by Apulis Technology (Shenzhen) Co. LTD
#
# This program and the accompanying materials are made available under the
# terms of the MIT License, which is available at
# https://www.opensource.org/licenses/MIT
#
# See the NOTICE file distributed with this work for additional
# information regarding copyright ownership.
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# SPDX-License-Identifier: MIT
############################################################################

import argparse
import os
import shutil

parser = argparse.ArgumentParser()
# Triton
parser.add_argument('--model_path', help='The model_path', required=True)
# source format onnx、saved_model、trt、air
parser.add_argument('--source_format', default="saved_model", help='The source platform', required=True)
parser.add_argument('--output_path', default=".", help='The output_path', required=True)
# 上面为必填
parser.add_argument('--max_batch_size', default=0, help="the max_batch_size", required=False)
parser.add_argument('--model_name', default="infer_model", help="the model name", required=False)

# OM
parser.add_argument('--model', help='The model', required=False)
parser.add_argument('--weight', help='The model', required=False)
parser.add_argument('--output', help='The output', required=False)
parser.add_argument('--soc_version', default="Ascend310", help='The soc_version', required=False)
parser.add_argument('--framework', default=1, help='The framework', required=False)
# 上面为必填
parser.add_argument('--input_format', help='The input_format', required=False)
parser.add_argument('--input_shape', help='The input_shape', required=False)
parser.add_argument('--insert_op_conf', help='The insert_op_conf', required=False)
parser.add_argument('--out_nodes', help='The out_nodes', required=False)
parser.add_argument('--input_fp16_nodes', help='The input_fp16_nodes', required=False)
parser.add_argument('--op_name_map', help='The op_name_map', required=False)
parser.add_argument('--precision_mode', help='The precision_mode', required=False)
parser.add_argument('--singleop', help='The singleop', required=False)

args, _ = parser.parse_known_args()

onnx_to_trition = {'tensor(float16)': 'TYPE_FP16', 'tensor(float)': 'TYPE_FP32', 'tensor(double)': 'TYPE_FP64',
                   'tensor(int32)': 'TYPE_INT32',
                   'tensor(int8)': 'TYPE_INT8',
                   'tensor(uint8)': 'TYPE_UINT8',
                   'tensor(int16)': 'TYPE_INT16',
                   'tensor(uint16)': 'TYPE_UINT16',
                   'tensor(int64)': 'TYPE_INT64',
                   'tensor(uint64)': 'TYPE_UINT64'
                   }


def onnx2Triton():
    import onnxruntime
    output_dir = os.path.join(args.output_path, args.model_name)
    hrnetw44_session = onnxruntime.InferenceSession(args.model_path)
    inputs = hrnetw44_session.get_inputs()
    outputs = hrnetw44_session.get_outputs()

    triton_config_lines = [f'name: "{args.model_name}"']
    triton_config_lines.append('platform: "onnxruntime_onnx"')
    for input in inputs:
        triton_config_lines.append('input')
        triton_config_lines.append('{')
        triton_config_lines.append(f'name:"{input.name}"')
        triton_config_lines.append(f'data_type:{onnx_to_trition.get(input.type)}')
        input_shape = input.shape
        input_shape = [-1 if isinstance(sh, str) else sh for sh in input_shape]
        triton_config_lines.append(f'dims:{input_shape}')
        triton_config_lines.append('}')
    for output in outputs:
        triton_config_lines.append('output')
        triton_config_lines.append('{')
        triton_config_lines.append(f'name:"{output.name}"')
        triton_config_lines.append(f'data_type:{onnx_to_trition.get(output.type)}')
        output_shape = output.shape
        output_shape = [-1 if isinstance(sh, str) else sh for sh in output_shape]
        triton_config_lines.append(f'dims:{output_shape}')
        triton_config_lines.append('}')
    output_saved_model = os.path.join(output_dir, "1")
    if os.path.exists(output_dir):
        shutil.rmtree(output_dir, ignore_errors=True)
    os.makedirs(output_saved_model)
    shutil.copy(args.model_path, os.path.join(output_saved_model, "model.onnx"))
    with open(os.path.join(output_dir, "config.pbtxt"), "w") as f:
        for line in triton_config_lines:
            f.write(line + "\n")


def savedModel2Triton():
    from google.protobuf import text_format
    import model_config_pb2
    from tensorflow.core.framework import types_pb2
    from tensorflow.python.tools import saved_model_utils
    tf_to_trition = {}
    tf_to_trition[types_pb2.DataType.DT_INVALID] = model_config_pb2.DataType.Value("TYPE_INVALID")
    tf_to_trition[types_pb2.DataType.DT_BOOL] = model_config_pb2.DataType.Value("TYPE_BOOL")

    tf_to_trition[types_pb2.DataType.DT_UINT8] = model_config_pb2.DataType.Value("TYPE_UINT8")
    tf_to_trition[types_pb2.DataType.DT_UINT16] = model_config_pb2.DataType.Value("TYPE_UINT16")
    tf_to_trition[types_pb2.DataType.DT_UINT32] = model_config_pb2.DataType.Value("TYPE_UINT32")
    tf_to_trition[types_pb2.DataType.DT_UINT64] = model_config_pb2.DataType.Value("TYPE_UINT64")

    tf_to_trition[types_pb2.DataType.DT_INT8] = model_config_pb2.DataType.Value("TYPE_INT8")
    tf_to_trition[types_pb2.DataType.DT_INT16] = model_config_pb2.DataType.Value("TYPE_INT16")
    tf_to_trition[types_pb2.DataType.DT_INT32] = model_config_pb2.DataType.Value("TYPE_INT32")
    tf_to_trition[types_pb2.DataType.DT_INT64] = model_config_pb2.DataType.Value("TYPE_INT64")

    tf_to_trition[types_pb2.DataType.DT_HALF] = model_config_pb2.DataType.Value("TYPE_FP16")
    tf_to_trition[types_pb2.DataType.DT_FLOAT] = model_config_pb2.DataType.Value("TYPE_FP32")
    tf_to_trition[types_pb2.DataType.DT_DOUBLE] = model_config_pb2.DataType.Value("TYPE_FP64")
    tf_to_trition[types_pb2.DataType.DT_STRING] = model_config_pb2.DataType.Value("TYPE_STRING")

    output_dir = os.path.join(args.output_path, args.model_name)
    model_dir = os.path.dirname(args.model_path)
    print(model_dir)
    # pb上一层
    tag_sets = saved_model_utils.get_saved_model_tag_sets(model_dir)
    model_config = {}
    type_map = {value: key for (key, value) in types_pb2.DataType.items()}
    triton_model_config = model_config_pb2.ModelConfig()
    triton_model_config.name = "infer_model"
    triton_model_config.platform = "tensorflow_savedmodel"
    triton_model_config.backend = "tensorflow"
    version_policy = model_config_pb2.ModelVersionPolicy()
    triton_model_config.version_policy.latest.num_versions = 1
    triton_model_config.max_batch_size = args.max_batch_size
    # triton_model_config.default_model_filename = "savedmodel"
    triton_model_config.default_model_filename = "infer_model"
    for tag_set in sorted(tag_sets):
        tag_set = ','.join(tag_set)
        model_config[tag_set] = {}
        signature_def_map = saved_model_utils.get_meta_graph_def(model_dir, tag_set)
        for signature_def_key in sorted(signature_def_map.signature_def.keys()):
            if signature_def_key == "__saved_model_init_op":
                continue
            model_config[tag_set][signature_def_key] = {}
            meta_graph_def = saved_model_utils.get_meta_graph_def(model_dir, tag_set)
            inputs_tensor_info = meta_graph_def.signature_def[signature_def_key].inputs
            outputs_tensor_info = meta_graph_def.signature_def[signature_def_key].outputs

            inputs = []
            outputs = []
            for input_key, input_tensor in sorted(inputs_tensor_info.items()):
                input = {}
                triton_inputs = model_config_pb2.ModelInput()
                triton_inputs.name = input_key
                input["name"] = input_key

                triton_inputs.data_type = tf_to_trition.get(input_tensor.dtype) or model_config_pb2.DataType.Value(
                    "TYPE_INVALID")
                input["dtype"] = type_map[input_tensor.dtype]

                input["dim"] = []
                for dim in input_tensor.tensor_shape.dim:
                    input["dim"].append(dim.size)
                    triton_inputs.dims.append(dim.size)
                inputs.append(input)
                triton_model_config.input.append(triton_inputs)

            for output_key, output_tensor in sorted(outputs_tensor_info.items()):
                output = {}
                triton_outputs = model_config_pb2.ModelOutput()
                output["name"] = output_key
                triton_outputs.name = output_key

                output["dtype"] = type_map[output_tensor.dtype]
                triton_outputs.data_type = tf_to_trition.get(output_tensor.dtype) or model_config_pb2.DataType.Value(
                    "TYPE_INVALID")

                output["dim"] = []
                for dim in output_tensor.tensor_shape.dim:
                    output["dim"].append(dim.size)
                    triton_outputs.dims.append(dim.size)
                outputs.append(output)
                triton_model_config.output.append(triton_outputs)

            model_config[tag_set][signature_def_key]["inputs"] = inputs
            model_config[tag_set][signature_def_key]["outputs"] = outputs
            break

    triton_model_config = text_format.MessageToString(triton_model_config, use_short_repeated_primitives=True)
    # infer/infer_model/1/infer_model/model.pb
    output_saved_model = os.path.join(output_dir, "1", args.model_name)
    if os.path.exists(output_dir):
        shutil.rmtree(output_dir, ignore_errors=True)
    os.makedirs(output_saved_model)
    with open(os.path.join(output_dir, "config.pbtxt"), "w") as f:
        f.write(triton_model_config)
    print(triton_model_config)
    # shutil.copytree(os.path.join(args.model_path, "export", "1"), os.path.join(output_path))
    # model_dir='ssd-triton\\infer\\models\\saved_model\\1'
    copy_all(os.path.join(model_dir), output_saved_model)
    return 0


def copy_all(src, dst):
    names = os.listdir(src)
    # 目标文件夹不存在，则新建
    if not os.path.exists(dst):
        os.mkdir(dst)
    for name in names:
        srcname = os.path.join(src, name)
        dstname = os.path.join(dst, name)
        if os.path.isdir(srcname):
            copy_all(srcname, dstname)
        else:
            if (not os.path.exists(dstname) or (
                    (os.path.exists(dstname)) and (os.path.getsize(dstname) != os.path.getsize(srcname)))):
                shutil.copy2(srcname, dst)


def concat_cmd(param):
    if getattr(args, param):
        return f"--{param} {getattr(args, param)}"
    return ""


def air2om():
    # model_dir = os.path.dirname(args.model_path)
    # for model in os.listdir(model_dir):
    # caffee
    if args.model_path.endswith("caffemodel"):
        args.weight = args.model_path
        args.framework = 0
    else:
        args.model = args.model_path
    if args.model_path.endswith("air"):
        args.framework = 1
    if args.model_path.endswith("pb"):
        args.framework = 3
    if args.model_path.endswith("onnx"):
        args.framework = 5
    if args.model_path.endswith("prototxt"):
        args.framework = 0
    # 输出模型跟输入名字一样
    dirname = os.path.dirname(args.model_path)
    model_name = os.path.splitext(os.path.basename(args.model_path))[0]
    args.output_path = os.path.join(dirname,"transformer",model_name)
    print(args.output_path)
    cmd = f"atc {concat_cmd('model')}  {concat_cmd('weight')}  {concat_cmd('singleop')} --output {args.output_path}  {concat_cmd('soc_version')}  {concat_cmd('framework')}  {concat_cmd('input_format')}  {concat_cmd('input_shape')}   {concat_cmd('insert_op_conf')}  {concat_cmd('out_nodes')}  {concat_cmd('input_fp16_nodes')}  {concat_cmd('op_name_map')}   {concat_cmd('precision_mode')} "
    print(cmd)
    res = os.popen(cmd).read()
    for b in res.split("\n"):
        print(b)


if __name__ == "__main__":
    print(30 * "=")
    print(f"start convert model to  {args.source_format}")
    print(args)
    if args.source_format == "saved_model":
        args.output_path = os.path.join(args.output_path, "models")
        savedModel2Triton()
    elif args.source_format == "onnx":
        args.output_path = os.path.join(args.output_path, "models")
        onnx2Triton()
    elif args.source_format == "om":
        air2om()

    print(f"finish convert model to  {args.source_format}")
