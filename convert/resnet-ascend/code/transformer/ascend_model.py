import base64
import time

import MxpiDataType_pb2 as MxpiDataType
import cv2
import numpy as np
from StreamManagerApi import StreamManagerApi, MxDataInput, InProtobufVector, MxProtobufIn, StringVector
from nms import soft_nms

def pre_process(image):
    # mean = np.array([0.408, 0.447, 0.470], dtype=np.float32).reshape((1, 1, 3))
    # std = np.array([0.289, 0.274, 0.278], dtype=np.float32).reshape((1, 1, 3))
    # height, width = image.shape[0:2]
    # new_height = int(height * scale)
    # new_width = int(width * scale)
    # inp_height, inp_width = 832, 832
    # c = np.array([new_width / 2., new_height / 2.], dtype=np.float32)
    # s = max(height, width) * 1.0
    # trans_input = get_affine_transform(c, s, 0, [inp_width, inp_height])
    resized_image = cv2.resize(image, dsize=(224, 224))
    data_set = resized_image*1.0/255.0

    i1 = (data_set[:, :, 0] - 0.4914) / 0.2023
    i2 = (data_set[:, :, 1] - 0.4822) / 0.1994
    i3 = (data_set[:, :, 2] - 0.4465) / 0.2010
    data_set = np.stack([i1, i2, i3], axis=2)
    data_set[:,:,::-1].transpose((2,0,1))
    data_set = np.float32(data_set)
    images = data_set.transpose(
        2, 0, 1).reshape(
        1, 3, 224, 224)
    # meta = {'c': c, 's': s, 'out_height': inp_height // 4,
    #         'out_width': inp_width // 4}
    return images

def softmax(x, axis=None):
    x = x - x.max(axis=axis, keepdims=True)
    y = np.exp(x) 
    return y / y.sum(axis=axis, keepdims=True)

def run(inputs_arr, stream_manager,stream_name):
    '''
    :param
        inputs_arr [{"data":"base64"}]
    :return:
        result_list list
        time_used   string
    '''
    result_list = []
    for input in inputs_arr:
        image_content = input["data"]
        image_bytes = base64.b64decode(image_content)
        image_bytes = np.fromstring(image_bytes, np.uint8)
        image_bytes = cv2.imdecode(image_bytes, cv2.IMREAD_COLOR)
        # print('image_bytes', image_bytes)
        images = pre_process(image_bytes)
        images = images.astype(np.float32)
        array_bytes = images.tobytes()
        inPluginId = 0
        dataInput = MxDataInput()
        dataInput.data = array_bytes
        tensorPackageList = MxpiDataType.MxpiTensorPackageList()
        tensorPackage = tensorPackageList.tensorPackageVec.add()
        tensorVec = tensorPackage.tensorVec.add()
        tensorVec.deviceId = 0
        tensorVec.memType = 0
        tensorVec.tensorShape.append(1)
        tensorVec.tensorShape.append(3)
        tensorVec.tensorShape.append(224)
        tensorVec.tensorShape.append(224)
        tensorVec.dataStr = array_bytes
        tensorVec.tensorDataSize = (len(array_bytes))
        key = b"appsrc0"
        protobufVec = InProtobufVector()
        protobuf = MxProtobufIn() # 推理业务输入数据定义
        protobuf.key = key
        protobuf.type = b'MxTools.MxpiTensorPackageList'
        protobuf.protobuf = tensorPackageList.SerializeToString()
        protobufVec.push_back(protobuf)
        uniqueId = stream_manager.SendProtobuf(stream_name, inPluginId, protobufVec)
        keyVec = StringVector()
        keyVec.push_back(b'mxpi_tensorinfer0')
        inferResult = stream_manager.GetProtobuf(stream_name, 0, keyVec)
        if inferResult.size() == 0:
            print("inferResult is null")
            exit()
        result = MxpiDataType.MxpiTensorPackageList()
        result.ParseFromString(inferResult[0].messageBuf)
        # print('result', result)
        x = np.frombuffer(result.tensorPackageVec[0].tensorVec[0].dataStr, dtype=np.float32).reshape((1, 10))
        # print(x)
        confidence = softmax(x)
        # print(confidence[0])
        position = np.argmax(x)
        dict={0:'airplane',1:'automobile',2:'bird',3:'cat',4:'deer',5:'dog',6:'frog',7:'horse',8:'skip',9:'truck'} 
        print(dict[position])
        # output_wh = np.frombuffer(result.tensorPackageVec[0].tensorVec[1].dataStr, dtype=np.float32).reshape(
        #     (1, 2, 208, 208))
        # output_off = np.frombuffer(result.tensorPackageVec[0].tensorVec[2].dataStr, dtype=np.float32).reshape(
        #     (1, 2, 208, 208))
        # output_kps = np.frombuffer(result.tensorPackageVec[0].tensorVec[3].dataStr, dtype=np.float32).reshape(
        #     (1, 10, 208, 208))
        # topk_inds = np.frombuffer(result.tensorPackageVec[0].tensorVec[4].dataStr, dtype=np.int32).reshape((1, 200))
        # dets = centerface_decode(
        #     topk_scores,
        #     output_wh,
        #     output_kps,
        #     reg=output_off,
        #     opt_k=200,
        #     topk_inds=topk_inds)
        # dets = dets.reshape(1, -1, dets.shape[2])
        # detections = []
        # dets = multi_pose_post_process(
        #     dets.copy(), [meta['c']], [meta['s']],
        #     meta['out_height'], meta['out_width'])
        # for j in range(1, 1 + 1):
        #     dets[0][j] = np.array(dets[0][j], dtype=np.float32).reshape(-1, 15)
        #     dets[0][j][:, :4] /= 1
        #     dets[0][j][:, 5:] /= 1
        # detections.append(dets[0])
        # output = merge_outputs(detections)
        MxpiObject = []
        # for bbox in output[1]:
        MxpiObject.append({
                "classVec": [
                    {
                        "classId": position,
                        "className": dict[position],
                        "confidence": float(confidence[0][position]),
                        "headerVec": []
                    }
                ]
            })
        result_list.append({"MxpiObject": MxpiObject})
    return result_list


if __name__ == '__main__':
    # import os
    # os.system("bash dependency.sh")
    stream_manager = StreamManagerApi()
    ret = stream_manager.InitManager()
    pipeline_config_file = "resnet50.pipeline"
    with open(pipeline_config_file, "rb")as pipeline_file:
        pipeline_bytes = pipeline_file.read()
    ret = stream_manager.CreateMultipleStreams(pipeline_bytes)
    with open("airplane.jpg", 'rb') as f:
        result_list = run([{"data": base64.b64encode(f.read()).decode()}], stream_manager, b"im_resnet50")
        print(result_list)
