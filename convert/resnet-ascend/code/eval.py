# Copyright 2020 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
"""train resnet."""
import os
import argparse
import json
from mindspore import context
from mindspore.common import set_seed
from mindspore.nn.loss import SoftmaxCrossEntropyWithLogits
from mindspore.train.model import Model
from mindspore.train.serialization import load_checkpoint, load_param_into_net
from src.CrossEntropySmooth import CrossEntropySmooth

parser = argparse.ArgumentParser(description='Image classification')
parser.add_argument('--net', type=str, default="resnet50", help='Resnet Model, either resnet50 or resnet101')
parser.add_argument('--dataset', type=str, default="cifar10", help='Dataset, either cifar10 or imagenet2012')

parser.add_argument('--checkpoint_path', type=str, default=None, help='Checkpoint file path')
parser.add_argument('--dataset_path', type=str, default=None, help='Dataset path')
parser.add_argument('--data_path', type=str, default=None, help='Dataset path')
parser.add_argument('--output_path', type=str, default=None, help='output path')
parser.add_argument('--device_target', type=str, default='Ascend', choices=("Ascend", "GPU", "CPU"),
                    help="Device target, support Ascend, GPU and CPU.")
args_opt = parser.parse_args()

set_seed(1)

if args_opt.net == "resnet50":
    from src.resnet import resnet50 as resnet
    if args_opt.dataset == "cifar10":
        from src.config import config1 as config
        from src.dataset import create_dataset1 as create_dataset
    else:
        from src.config import config2 as config
        from src.dataset import create_dataset2 as create_dataset
elif args_opt.net == "resnet101":
    from src.resnet import resnet101 as resnet
    from src.config import config3 as config
    from src.dataset import create_dataset3 as create_dataset
else:
    from src.resnet import se_resnet50 as resnet
    from src.config import config4 as config
    from src.dataset import create_dataset4 as create_dataset

if __name__ == '__main__':
    target = args_opt.device_target
    args_opt.dataset_path = args_opt.data_path
    # init context
    context.set_context(mode=context.GRAPH_MODE, device_target=target, save_graphs=False)
    if target == "Ascend":
        device_id = int(os.getenv('DEVICE_ID'))
        context.set_context(device_id=device_id)

    # create dataset
    dataset = create_dataset(dataset_path=args_opt.dataset_path, do_train=False, batch_size=config.batch_size,
                             target=target)
    step_size = dataset.get_dataset_size()

    # define net
    net = resnet(class_num=config.class_num)

    # load checkpoint
    if args_opt.checkpoint_path:
        if args_opt.checkpoint_path.endswith(".ckpt"):
            param_dict = load_checkpoint(args_opt.checkpoint_path)
            load_param_into_net(net, param_dict)
            net.set_train(False)
        else:
            files = os.listdir(args_opt.checkpoint_path)
            ckpt_model_list = [file for file in files if file.endswith(".ckpt")]
            ckpt_model = os.path.join(args_opt.checkpoint_path, sorted(ckpt_model_list)[-1])
            param_dict = load_checkpoint(ckpt_model)
            load_param_into_net(net, param_dict)
            net.set_train(False)
    else:
        print("Checkpoint is None")
    # define loss, model
    if args_opt.dataset == "imagenet2012":
        if not config.use_label_smooth:
            config.label_smooth_factor = 0.0
        loss = CrossEntropySmooth(sparse=True, reduction='mean',
                                  smooth_factor=config.label_smooth_factor, num_classes=config.class_num)
    else:
        loss = SoftmaxCrossEntropyWithLogits(sparse=True, reduction='mean')

    # define model
    model = Model(net, loss_fn=loss, metrics={'top_1_accuracy', 'top_5_accuracy'})

    # eval model
    res = model.eval(dataset)
    print("result:", res, "ckpt=", args_opt.checkpoint_path)
    # print(res['top_1_accuracy'])
    with open(os.path.join(args_opt.output_path,"eval_result.json"),"w")as f:
        # json.dump({"top_1_accuracy":res["top_1_accuracy"],"top_5_accuracy":res["top_5_accuracy"]},f)
        json.dump({
            "name": f"resnet50-classification-mindspore",
            "evaluation": [
                {
                    "key": "top_1_accuracy",
                    "value": str(res["top_1_accuracy"]),
                    "type": "string",
                    "desc": "排名第一的类别与实际结果相符的准确率"
                },
                {
                    "key": "top_5_accuracy",
                    "value": str(res["top_5_accuracy"]),
                    "type": "string",
                    "desc": "排名前五的类别包含实际结果的准确率"
                }
            ]
        }, f)