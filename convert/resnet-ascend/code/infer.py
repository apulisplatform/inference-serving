# Copyright 2020 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
"""infer resnet."""
import os
import sys
import argparse
import mindspore
import numpy as np
import cv2
from mindspore import context
from mindspore.common.tensor import Tensor
from mindspore.common import set_seed
from mindspore.nn.loss import SoftmaxCrossEntropyWithLogits
from mindspore.train.model import Model
from mindspore.train.serialization import load_checkpoint, load_param_into_net
from src.CrossEntropySmooth import CrossEntropySmooth
from src.resnet import resnet50 as resnet
from src.config import config1 as config
from src.dataset import create_dataset1 as create_dataset
target = 'Ascend'
# init context
context.set_context(mode=context.GRAPH_MODE, device_target=target, save_graphs=False)
# device_id = int(os.getenv('DEVICE_ID'))
device_id = 7
context.set_context(device_id=device_id)
# define net
net = resnet(class_num=config.class_num)

# create dataset
data_set = cv2.imread('/mnt/deploy/admin/fengchong/threec/resnet-527/dog2.jpg')
data_set = cv2.resize(data_set, dsize=(224, 224))
data_set = data_set*1.0/255.0 

i1 = (data_set[:, :, 0] - 0.4914) / 0.2023
i2 = (data_set[:, :, 1] - 0.4822) / 0.1994
i3 = (data_set[:, :, 2] - 0.4465) / 0.2010
data_set = np.stack([i1, i2, i3], axis=2)

data_set[:,:,::-1].transpose((2,0,1))
data_set = np.float32(data_set)
data_set = data_set.transpose(
        2, 0, 1).reshape(
        1, 3, 224, 224)
data_set = Tensor(data_set)

# load checkpoint
param_dict = load_checkpoint('/mnt/deploy/admin/fengchong/threec/resnet-527/scripts/train/resnet-90_1875.ckpt')
load_param_into_net(net, param_dict)
net.set_train(False)

# define loss
loss = SoftmaxCrossEntropyWithLogits(sparse=True, reduction='mean')

# define model
model = Model(net, loss_fn=loss, metrics={'top_1_accuracy', 'top_5_accuracy'})

x = model.predict(data_set)
position = np.argmax(x)
dict={0:'airplane',1:'automobile',2:'bird',3:'cat',4:'deer',5:'dog',6:'frog',7:'horse',8:'skip',9:'truck'} 
print(dict[position])