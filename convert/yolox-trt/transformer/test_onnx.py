import json
import os
import time

from PIL import Image
from tritonclient.utils import *
import tritonclient.http as httpclient


def transform(image):
    image= image.resize((416,416))
    image = np.asarray(image).astype(np.float32)
    image = np.transpose(image, [2, 0, 1])
    image = image / 255
    # image = np.expand_dims(image, 0)
    return image

check_img = './009_3_NG62_B_2_1936.5_3692.5_check.bmp'

# compute ONNX Runtime output prediction
check = transform(Image.open(check_img).convert("RGB"))


with httpclient.InferenceServerClient("localhost:8000") as client:
    start = time.time()
    inputs = [
        httpclient.InferInput("input_0", check.shape,
                              np_to_triton_dtype(check.dtype)),
    ]
    inputs[0].set_data_from_numpy(check)
    # for i in range(1000):
    result = client.infer("tensorflow_model", inputs)
    output0 = result.as_numpy('output_0')
    print(output0)
    end=time.time()
    print(end-start)
    print(1000/(end-start))


