import base64
import logging
from typing import Dict

import cv2
import kfserving
import numpy as np
import tritonclient.http as httpclient


class InferModel(kfserving.KFModel):
    def __init__(self, name: str, predictor_host: str):
        super().__init__(name)
        self.load()
        self.triton_client = None
        self.model_parser = None
        self.predictor_host = predictor_host

    def to_numpy(self, tensor):
        return tensor.detach().cpu().numpy() if tensor.requires_grad else tensor.cpu().numpy()

    def preprocess(self, inputs: Dict) -> Dict:
        # 会自动修改inputs
        model_parser = self.get_model_metadata()
        for input in inputs["inputs"]:
            images_data = input["data"]
            images_data = base64.b64decode(images_data)
            images_data = np.asarray(bytearray(images_data), dtype="uint8")
            image = cv2.imdecode(images_data, cv2.IMREAD_COLOR)
            width = 512
            height = 512
            dtype = model_parser["inputs"][0]["datatype"]
            input_name = model_parser["inputs"][0]["name"]
            need_transpose = False
            for index, shape in enumerate(model_parser["inputs"][0]["shape"]):
                if index == 1 and shape == 3:
                    need_transpose = True
                if shape != -1 and shape != 1 and shape != 3:
                    width = shape
                    height = shape
            image = cv2.resize(image, (width, height))
            logging.error(image.shape)
            logging.error(dtype)
            if "FLOAT" in dtype or "FP" in dtype:
                image = np.asarray(image).astype(np.float32)
                image = image / 255
                logging.error(image.shape)

            if "conv2d" in input_name:
                image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
                image = np.expand_dims(image, axis=2)
            # 如果最后一位不是3则要transpose
            if need_transpose:
                print(image.shape)
                image = np.transpose(image, [2, 0, 1])
            image = np.expand_dims(image, 0)
            input["data"] = image
        return inputs

    def predict(self, inputs: Dict) -> Dict:
        request_id = ""
        model_parser = self.get_model_metadata()
        logging.error(model_parser)
        new_inputs = []
        for idx, input in enumerate(inputs["inputs"]):
            input_data = input["data"]
            node = model_parser["inputs"][idx]
            name, shape, dtype = node["name"], node["shape"], node["datatype"]
            logging.error(name, shape, dtype)
            new_inputs.append(httpclient.InferInput(name, input_data.shape, dtype))
            new_inputs[idx].set_data_from_numpy(input_data)
        outputs = []
        for idx in range(len(model_parser["outputs"])):
            node = model_parser["outputs"][idx]
            output = httpclient.InferRequestedOutput(node["name"])
            outputs.append(output)
        logging.error("begin infer")
        logging.error(model_parser["name"])

        result = self.triton_client.infer(model_parser["name"], new_inputs, outputs=outputs, request_id=request_id)
        logging.error("after infer")
        return result

    def postprocess(self, result: Dict) -> Dict:
        postprocess_result = result.get_response()
        model_parser = self.get_model_metadata()
        outputs = []
        for idx in range(len(model_parser["outputs"])):
            node = model_parser["outputs"][idx]
            logging.error(node)
            res_data = result.as_numpy(node["name"])
            res_data_shape = list(res_data.shape)
            res_data_dtype = str(res_data.dtype)
            if (True in np.isnan(res_data)):
                res_data = np.asarray([])
            outputs.append({'name': node["name"], 'datatype': res_data_dtype,
                            'shape': res_data_shape,
                            "data": res_data.tolist()})
        postprocess_result['outputs'] = outputs
        return postprocess_result

    def init_client(self):
        if not self.triton_client:
            self.triton_client = httpclient.InferenceServerClient(
                url=self.predictor_host, verbose=False)

    def get_config(self):
        self.init_client()
        return self.triton_client.get_model_config(model_name=self.name)

    def get_model_metadata(self):
        self.init_client()
        return self.triton_client.get_model_metadata(model_name=self.name)
