import base64

import requests

check_img = 'cat.0.jpg'

with open(check_img, 'rb') as f:
    inputs = [{"data": base64.b64encode(f.read()).decode()}]

res = requests.post("http://127.0.0.1:8080/v2/models/infer_model/infer", json={"inputs": inputs}).text
exit(0)

