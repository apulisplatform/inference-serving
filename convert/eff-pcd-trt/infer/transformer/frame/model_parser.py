import tritonclient.grpc.model_config_pb2 as mc

class ModelParser:
    def __init__(self,model_metadata,model_config):
        self.model_metadata = model_metadata
        self.model_config   = model_config

    def input_shape(self,index):
        inputs = self.model_config['input']
        max_batch_size = self.max_batch_size()

        if index >= len(inputs):
            raise Exception("inputs has {} elems,get {}'th error".format(len(inputs),index))

        input_info = inputs[index]
        input_config = self.model_metadata["inputs"][index]
        input_batch_dim = (max_batch_size > 0)
        expected_input_dims = 3 + (1 if input_batch_dim else 0)

        if type(input_info['format']) == str:
            FORMAT_ENUM_TO_INT = dict(mc.ModelInput.Format.items())
            input_info['format'] = FORMAT_ENUM_TO_INT[input_info['format']]

        if (len(input_config["shape"]) != expected_input_dims) and input_info['format']:
            raise Exception("expecting input to have {} dimensions, model '{}' input has {}".
            format(expected_input_dims, self.model_metadata['name'],len(input_config["shape"])))


        if ((input_info['format'] != mc.ModelInput.FORMAT_NCHW) and
                (input_info['format'] != mc.ModelInput.FORMAT_NHWC)):
            raise Exception("unexpected input format " + 
                    mc.ModelInput.Format.Name(input_info['format']) + ", expecting " + 
                    mc.ModelInput.Format.Name(mc.ModelInput.FORMAT_NCHW) + " or " + 
                    mc.ModelInput.Format.Name(mc.ModelInput.FORMAT_NHWC))

        c = 0
        h = 0
        w = 0
        if input_info['format'] == mc.ModelInput.FORMAT_NHWC:
            h = input_config['shape'][1 if input_batch_dim else 0]
            w = input_config['shape'][2 if input_batch_dim else 1]
            c = input_config['shape'][3 if input_batch_dim else 2]
        else:
            c = input_config['shape'][1 if input_batch_dim else 0]
            h = input_config['shape'][2 if input_batch_dim else 1]
            w = input_config['shape'][3 if input_batch_dim else 2]
        
        return c,h,w,input_info['format'],input_config['datatype'],input_info['name']

    def input_name(self,idx):
        return self.model_config['input'][idx]['name']

    def input_dtype(self,idx):
        return self.model_metadata['inputs'][idx]['datatype']

    def max_batch_size(self):
        return self.model_config['max_batch_size']

    def input_tensor_size(self):
        return len(self.model_config['input'])

    def output_tensor_size(self):
        return len(self.model_config['output'])

    def model_name(self):
        return self.model_metadata["name"]

    def output_name(self,idx):
        outputs = self.model_metadata["outputs"]

        if (idx >= len(outputs)):
            raise Exception("outputs has {} elems,get {}'th error".format(len(outputs),idx))
        return outputs[idx]["name"]

    def parse(self):
        inputs = self.model_metadata['inputs']
        outputs = self.model_metadata['outputs']

        print(self.input_shape(0))

        if len(inputs) != 1:
            raise Exception("expecting 1 input, got {}".format(len(inputs)))
        if len(outputs) != 1:
            raise Exception("expecting 1 output, got {}".format(len(outputs)))
