import kfserving
from kfserving.kfserver import *
from tritonclient.utils import InferenceServerException
from http import HTTPStatus
from cloudevents.sdk.converters.util import has_binary_headers

class KFServerV2(kfserving.KFServer):
    def __init__(self):
        super().__init__()

    def create_application(self):
        return tornado.web.Application([
            # Server Liveness API returns 200 if server is alive.
            (r"/", LivenessHandler),
            (r"/v2/health/live", LivenessHandler),
            (r"/v1/models",
                ListHandler, dict(models=self.registered_models)),
            (r"/v2/models",
                ListHandler, dict(models=self.registered_models)),
            # Model Health API returns 200 if model is ready to serve.
            (r"/v1/models/([a-zA-Z0-9_-]+)",
                HealthHandler, dict(models=self.registered_models)),
            (r"/v2/models/([a-zA-Z0-9_-]+)/status",
                HealthHandler, dict(models=self.registered_models)),
            (r"/v2/models/([a-zA-Z0-9_-]+)",
                MetaDataHandler, dict(models=self.registered_models)),
            (r"/v2/models/([a-zA-Z0-9_-]+)/config",
                ConfigHandler, dict(models=self.registered_models)),
            (r"/v1/models/([a-zA-Z0-9_-]+):predict",
                PredictHandler, dict(models=self.registered_models)),
            (r"/v2/models/([a-zA-Z0-9_-]+)/infer",
                PredictHandler, dict(models=self.registered_models)),
            (r"/v1/models/([a-zA-Z0-9_-]+):explain",
                ExplainHandler, dict(models=self.registered_models)),
            (r"/v2/models/([a-zA-Z0-9_-]+)/explain",
                ExplainHandler, dict(models=self.registered_models)),
            (r"/v2/repository/models/([a-zA-Z0-9_-]+)/load",
                LoadHandler, dict(models=self.registered_models)),
            (r"/v2/repository/models/([a-zA-Z0-9_-]+)/unload",
                UnloadHandler, dict(models=self.registered_models)),
            ])

class PredictV2Handler(tornado.web.RequestHandler):
    def initialize(self, models: KFModelRepository):
        self.models = models

    def post(self,name : str):
        print(self.request.headers)
        if has_binary_headers(self.request.headers):
            print("has_binary_headers")
        print(self.request.body)


class MetaDataHandler(tornado.web.RequestHandler):
    def initialize(self, models: KFModelRepository):
        self.models = models  # pylint:disable=attribute-defined-outside-init

    def get(self,name : str):
        print(self.request,name)
        model = self.models.get_model(name)
        try:
            response = model.get_model_metadata()
            self.write(response)
        except Exception as e:
            raise tornado.web.HTTPError(
                status_code=HTTPStatus.BAD_REQUEST,
                reason="ConfigHandler error : %s" % e
            )

class ConfigHandler(tornado.web.RequestHandler):
    def initialize(self, models: KFModelRepository):
        self.models = models  # pylint:disable=attribute-defined-outside-init

    def get(self,name : str):
        print(self.request,name)
        model = self.models.get_model(name)
        try:
            response = model.get_config()
            self.write(response)
        except Exception as e:
            raise tornado.web.HTTPError(
                status_code=HTTPStatus.BAD_REQUEST,
                reason="ConfigHandler error : %s" % e
            )
