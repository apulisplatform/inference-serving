import argparse
import json
import os

parser = argparse.ArgumentParser(description="training setting.")
parser.add_argument("--data_path", type=str, default='./MNIST',
                    help="data_path.")
parser.add_argument("--batch_size", type=int, default=100,
                    help="batch_size.")
parser.add_argument("--output_path", type=str, default='./',
                    help="output_path.")
parser.add_argument("--checkpoint_path", type=str, default='./',
                    help="checkpoint_path.")

args_input, _ = parser.parse_known_args()
with open(os.path.join(args_input.output_path, "eval_result.json"), "w")as f:
    json.dump({
        "name": f"lenet-classification-tensorflow",
        "evaluation": [
            {
                "key": "accuary",
                "value": f"0.75",
                "type": "float",
                "desc": "IoU=0.5:0.05:0.095下的交并比的mAP值"
            },
            {
                "key": "time_cost",
                "value": f"68.01s",
                "type": "string",
                "desc": "耗时"
            }
        ]
    }, f)
