import argparse
import json
import os

parser = argparse.ArgumentParser(description="training setting.")
parser.add_argument("--data_path", type=str, default='./MNIST',
                    help="data_path.")
parser.add_argument("--batch_size", type=int, default=100,
                    help="batch_size.")
parser.add_argument("--output_path", type=str, default='./',
                    help="output_path.")
parser.add_argument("--checkpoint_path", type=str, default='./',
                    help="checkpoint_path.")

args_input, _ = parser.parse_known_args()
print("finish train")