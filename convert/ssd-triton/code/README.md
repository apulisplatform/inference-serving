### 模型介绍

本模型是以SSD-mobilnet为框架进行目标检测网格训练。

### 任务介绍

各自在图像信息中所反映的不同特征，把不同类别的目标区分开来的图像处理方法。它利用计算机对图像进行定量分析，把图像或图像中的每个像元或区域划归为若干个类别中的某一种，以代替人的视觉判读。

### 数据集介绍

ImageNet图像数据集始于2009年，当时李飞飞教授等在CVPR2009上发表了一篇名为《ImageNet: A Large-Scale Hierarchical Image Database》的论文，之后就是基于ImageNet数据集的7届ImageNet挑战赛(2010年开始)，2017年后，ImageNet由Kaggle(Kaggle公司是由联合创始人兼首席执行官Anthony Goldbloom 2010年在墨尔本创立的，主要是为开发商和数据科学家提供举办机器学习竞赛、托管数据库、编写和分享代码的平台)继续维护。

### 运行方法

- Python 3.6+
- PyTorch 1.3+
- MMCV
- 单GPU训练 python tools/train.py ${CONFIG_FILE} [optional arguments]
- 多GPU训练 ./tools/dist_train.sh ${CONFIG_FILE} ${GPU_NUM} [optional arguments]

### 日志效果







