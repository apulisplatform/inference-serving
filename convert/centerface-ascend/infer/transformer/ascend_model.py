import base64
import time

import MxpiDataType_pb2 as MxpiDataType
import cv2
import numpy as np
from StreamManagerApi import StreamManagerApi, MxDataInput, InProtobufVector, MxProtobufIn, StringVector
from nms import soft_nms


def merge_outputs(detections):
    """
    Merge detection outputs
    """
    results = {}
    results[1] = np.concatenate(
        [detection[1] for detection in detections], axis=0).astype(np.float32)
    soft_nms(results[1], Nt=0.5, threshold=0.5, method=2)
    # results[1] = results[1].tolist()
    return results


def affine_transform(pt, t):
    """
    Affine transform
    """
    new_pt = np.array([pt[0], pt[1], 1.], dtype=np.float32).T
    new_pt = np.dot(t, new_pt)
    return new_pt[:2]


def get_3rd_point(a, b):
    direct = a - b
    return b + np.array([-direct[1], direct[0]], dtype=np.float32)


def transform_preds(coords, center, scale, output_size):
    """
    Transform target coords
    """
    target_coords = np.zeros(coords.shape)
    trans = get_affine_transform(center, scale, 0, output_size, inv=1)
    for p in range(coords.shape[0]):
        target_coords[p, 0:2] = affine_transform(coords[p, 0:2], trans)
    return target_coords


def multi_pose_post_process(dets, c, s, h, w):
    """
    Multi pose post process
    dets_result: 4 + score:1 + kpoints:10 + class:1 = 16
    dets: batch x max_dets x 40
    return list of 39 in image coord
    """
    ret = []
    for i in range(dets.shape[0]):
        bbox = transform_preds(
            dets[i, :, :4].reshape(-1, 2), c[i], s[i], (w, h))
        pts = transform_preds(
            dets[i, :, 5:15].reshape(-1, 2), c[i], s[i], (w, h))
        top_preds = np.concatenate([bbox.reshape(-1, 4), dets[i, :, 4:5], pts.reshape(-1, 10)],
                                   axis=1).astype(np.float32).tolist()
        ret.append({np.ones(1, dtype=np.int32)[0]: top_preds})
    return ret


def get_dir(src_point, rot_rad):
    """
    Get dir
    """
    sn, cs = np.sin(rot_rad), np.cos(rot_rad)  # (0, 1)

    src_result = [0, 0]
    src_result[0] = src_point[0] * cs - src_point[1] * sn
    src_result[1] = src_point[0] * sn + src_point[1] * cs

    return src_result


def get_affine_transform(center,
                         scale,
                         rot,
                         output_size,
                         shift=np.array([0, 0], dtype=np.float32),
                         inv=0):
    if not isinstance(scale, np.ndarray) and not isinstance(scale, list):
        scale = np.array([scale, scale], dtype=np.float32)

    scale_tmp = scale
    src_w = scale_tmp[0]
    dst_w = output_size[0]
    dst_h = output_size[1]

    rot_rad = np.pi * rot / 180
    src_dir = get_dir([0, src_w * -0.5], rot_rad)
    dst_dir = np.array([0, dst_w * -0.5], np.float32)

    src = np.zeros((3, 2), dtype=np.float32)
    dst = np.zeros((3, 2), dtype=np.float32)
    src[0, :] = center + scale_tmp * shift
    src[1, :] = center + src_dir + scale_tmp * shift
    dst[0, :] = [dst_w * 0.5, dst_h * 0.5]
    dst[1, :] = np.array([dst_w * 0.5, dst_h * 0.5], np.float32) + dst_dir

    src[2:, :] = get_3rd_point(src[0, :], src[1, :])
    dst[2:, :] = get_3rd_point(dst[0, :], dst[1, :])

    if inv:
        trans = cv2.getAffineTransform(np.float32(dst), np.float32(src))
    else:
        trans = cv2.getAffineTransform(np.float32(src), np.float32(dst))

    return trans


def centerface_decode(heat, wh, kps, reg=None,
                      opt_k=200, topk_inds=None):
    """
    Decode detection bbox
    """
    batch, _, _, width = wh.shape

    num_joints = kps.shape[1] // 2

    scores = heat
    inds = topk_inds.astype(np.int16)
    ys_int = (topk_inds / width).astype(np.int32)
    xs_int = (topk_inds % width).astype(np.int32)

    reg = reg.reshape(batch, 2, -1)
    reg_tmp = np.zeros((batch, 2, opt_k), dtype=np.float32)
    for i in range(batch):
        reg_tmp[i, 0, :] = reg[i, 0, inds[i]]
        reg_tmp[i, 1, :] = reg[i, 1, inds[i]]
    reg = reg_tmp.transpose(0, 2, 1)

    if reg is not None:
        xs = xs_int.reshape(batch, opt_k, 1) + reg[:, :, 0:1]
        ys = ys_int.reshape(batch, opt_k, 1) + reg[:, :, 1:2]
    else:
        xs = xs_int.reshape(batch, opt_k, 1) + 0.5
        ys = ys_int.reshape(batch, opt_k, 1) + 0.5

    wh = wh.reshape(batch, 2, -1)
    wh_tmp = np.zeros((batch, 2, opt_k), dtype=np.float32)
    for i in range(batch):
        wh_tmp[i, 0, :] = wh[i, 0, inds[i]]
        wh_tmp[i, 1, :] = wh[i, 1, inds[i]]

    wh = wh_tmp.transpose(0, 2, 1)
    wh = np.exp(wh) * 4.
    scores = scores.reshape(batch, opt_k, 1)
    bboxes = np.concatenate([xs - wh[..., 0:1] / 2, ys - wh[..., 1:2] / 2, xs + wh[..., 0:1] / 2,
                             ys + wh[..., 1:2] / 2], axis=2)

    clses = np.zeros((batch, opt_k, 1), dtype=np.float32)
    kps = np.zeros((batch, opt_k, num_joints * 2), dtype=np.float32)
    # box:4+score:1+kpoints:10+class:1=16
    detections = np.concatenate([bboxes, scores, kps, clses], axis=2)
    return detections


def pre_process(image, scale):
    mean = np.array([0.408, 0.447, 0.470], dtype=np.float32).reshape((1, 1, 3))
    std = np.array([0.289, 0.274, 0.278], dtype=np.float32).reshape((1, 1, 3))
    height, width = image.shape[0:2]
    new_height = int(height * scale)
    new_width = int(width * scale)
    inp_height, inp_width = 832, 832
    c = np.array([new_width / 2., new_height / 2.], dtype=np.float32)
    s = max(height, width) * 1.0
    trans_input = get_affine_transform(c, s, 0, [inp_width, inp_height])
    resized_image = cv2.resize(image, (new_width, new_height))
    inp_image = cv2.warpAffine(
        resized_image, trans_input, (inp_width, inp_height),
        flags=cv2.INTER_LINEAR)
    inp_image = ((inp_image / 255. - mean) / std)
    images = inp_image.transpose(
        2, 0, 1).reshape(
        1, 3, inp_height, inp_width)
    meta = {'c': c, 's': s, 'out_height': inp_height // 4,
            'out_width': inp_width // 4}
    return images, meta


def run(inputs_arr, stream_manager,stream_name):
    '''
    :param
        inputs_arr [{"data":"base64"}]
    :return:
        result_list list
        time_used   string
    '''
    result_list = []
    for input in inputs_arr:
        image_content = input["data"]
        image_bytes = base64.b64decode(image_content)
        image_bytes = np.fromstring(image_bytes, np.uint8)
        image_bytes = cv2.imdecode(image_bytes, cv2.IMREAD_COLOR)
        images, meta = pre_process(image_bytes, 1)
        images = images.astype(np.float32)
        array_bytes = images.tobytes()
        inPluginId = 0
        dataInput = MxDataInput()
        dataInput.data = array_bytes
        tensorPackageList = MxpiDataType.MxpiTensorPackageList()
        tensorPackage = tensorPackageList.tensorPackageVec.add()
        tensorVec = tensorPackage.tensorVec.add()
        tensorVec.deviceId = 0
        tensorVec.memType = 0
        tensorVec.tensorShape.append(1)
        tensorVec.tensorShape.append(3)
        tensorVec.tensorShape.append(832)
        tensorVec.tensorShape.append(832)
        tensorVec.dataStr = array_bytes
        tensorVec.tensorDataSize = (len(array_bytes))
        key = b"appsrc0"
        protobufVec = InProtobufVector()
        protobuf = MxProtobufIn()
        protobuf.key = key
        protobuf.type = b'MxTools.MxpiTensorPackageList'
        protobuf.protobuf = tensorPackageList.SerializeToString()
        protobufVec.push_back(protobuf)
        uniqueId = stream_manager.SendProtobuf(stream_name, inPluginId, protobufVec)
        keyVec = StringVector()
        keyVec.push_back(b'mxpi_tensorinfer0')
        inferResult = stream_manager.GetProtobuf(stream_name, 0, keyVec)
        if inferResult.size() == 0:
            print("inferResult is null")
            exit()
        result = MxpiDataType.MxpiTensorPackageList()
        result.ParseFromString(inferResult[0].messageBuf)
        topk_scores = np.frombuffer(result.tensorPackageVec[0].tensorVec[0].dataStr, dtype=np.float32).reshape((1, 200))
        output_wh = np.frombuffer(result.tensorPackageVec[0].tensorVec[1].dataStr, dtype=np.float32).reshape(
            (1, 2, 208, 208))
        output_off = np.frombuffer(result.tensorPackageVec[0].tensorVec[2].dataStr, dtype=np.float32).reshape(
            (1, 2, 208, 208))
        output_kps = np.frombuffer(result.tensorPackageVec[0].tensorVec[3].dataStr, dtype=np.float32).reshape(
            (1, 10, 208, 208))
        topk_inds = np.frombuffer(result.tensorPackageVec[0].tensorVec[4].dataStr, dtype=np.int32).reshape((1, 200))
        dets = centerface_decode(
            topk_scores,
            output_wh,
            output_kps,
            reg=output_off,
            opt_k=200,
            topk_inds=topk_inds)
        dets = dets.reshape(1, -1, dets.shape[2])
        detections = []
        dets = multi_pose_post_process(
            dets.copy(), [meta['c']], [meta['s']],
            meta['out_height'], meta['out_width'])
        for j in range(1, 1 + 1):
            dets[0][j] = np.array(dets[0][j], dtype=np.float32).reshape(-1, 15)
            dets[0][j][:, :4] /= 1
            dets[0][j][:, 5:] /= 1
        detections.append(dets[0])
        output = merge_outputs(detections)
        MxpiObject = []
        for bbox in output[1]:
            MxpiObject.append({
                "classVec": [
                    {
                        "classId": 0,
                        "className": "face",
                        "confidence": float(bbox[4]),
                        "headerVec": []
                    }
                ],
                "x0": float(bbox[0]),
                "y0": float(bbox[1]),
                "x1": float(bbox[2]),
                "y1": float(bbox[3])
            })
        result_list.append({"MxpiObject": MxpiObject})
    return result_list


if __name__ == '__main__':
    import os
    os.system("bash dependency.sh")
    stream_manager = StreamManagerApi()
    ret = stream_manager.InitManager()
    pipeline_config_file = "../centerface.pipeline"
    with open(pipeline_config_file, "rb")as pipeline_file:
        pipeline_bytes = pipeline_file.read()
    ret = stream_manager.CreateMultipleStreams(pipeline_bytes)
    with open("test.png", 'rb') as f:
        result_list = run([{"data": base64.b64encode(f.read()).decode()}], stream_manager,b"im_centerface")
        print(result_list)
