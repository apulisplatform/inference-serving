# apflow-convert-tools
## 拉取转换镜像
```
ssh root@192.168.3.126
password kQ1sDC4mDZpojDo4
docker pull harbor.apulis.cn:8443/algorithm/apulistech/apflow-convert-tool:21.0.2-amd64

```
## om转换
```shell script
# 模型转换后会在当前目录下生成转换后的模型
export model_path=/home/xky/inference-serving/convert/ssd-ascend/infer/ssd.air
export output_path=/home/xky/inference-serving/convert/ssd-ascend/infer
docker run -it --rm --privileged -v ${output_path}:${output_path}  harbor.apulis.cn:8443/algorithm/apulistech/apflow-convert-tool:21.0.2-amd64 python /work/main.py --source_format=om  --model_path=${model_path} --output_path=${output_path}
```
 
## saved_model转Triton   
 
```shell script
export model_path=/home/xky/inference-serving/convert/ssd-face/infer/saved_model/1/saved_model.pb 
export output_path=/home/xky/inference-serving/convert/ssd-face/infer
docker run -it --rm --privileged -v ${output_path}:${output_path}  harbor.apulis.cn:8443/algorithm/apulistech/apflow-convert-tool:21.0.2-amd64 python /work/main.py  --source_format=saved_model  --model_path=${model_path}  --output_path=${output_path}

#test
# python main.py  --target_format=pbtxt  --model_path=/home/xky/inference-serving/convert/ssd_efficientdet_d0/infer/export/1/saved_model.pb   --output_path=/home/xky/inference-serving/convert/ssd_efficientdet_d0/infer

```


## KJ 推理全流程

- onnx转为triton
```shell script
ssh root@192.168.3.126

export model_dir=/home/xky/inference-serving/convert/resnet-openlab
export model_path=${model_dir}/infer/model.onnx 
export output_path=${model_dir}/infer
docker run -it --rm --privileged -v ${model_dir}:${model_dir}  harbor.apulis.cn:8443/algorithm/apulistech/apflow-convert-tool:21.0.2-amd64 python /work/main.py  --source_format=onnx  --model_path=${model_path}  --output_path=${output_path} 
```

- output_path下会生成models转换后的Triton模型 因此启动triton
```shell script
screen docker run  -it --gpus=1 --rm  -p8000:8000 -p8001:8001 -p8002:8002 -v ${model_dir}/infer/models:/models nvcr.io/nvidia/tritonserver:20.10-py3 ./bin/tritonserver --model-repository=/models --strict-model-config=false
```

- 编写code/transformer下的前后处理代码tensorflow_model.py，并在新终端中启动transformer镜像
```shell script
screen docker run --rm -v ${model_dir}/code/transformer:/Work/transformer  -p8080:8080 harbor.apulis.cn:8443/algorithm/apulistech/kfserving-transformer/trition_transformer --predictor_host 192.168.3.126:8000
```

- 新终端中执行测试
```shell script
cd ${model_dir}/code/transformer
python3 test_transfor.py
```



## 模型名称
- 模型文件夹名称
- 1/模型文件夹名称
- config.pbtxt中模型名称
- transformer启动文件中的路由    都应为 tensorflow_model

## savedmodel转换
- 转换后生成config.pbtxt
- 转换后生成1文件夹下modelname文件夹


