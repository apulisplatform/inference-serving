import torch.onnx
import argparse
import tkinter as tk

import torch

from isegm.utils import exp
from isegm.inference import utils
from interactive_demo.app import InteractiveDemoApp


def demo():
    import torch

    x_data = torch.Tensor([[1.0], [2.0], [3.0]])
    y_data = torch.Tensor([[2.0], [4.0], [6.0]])
    print(x_data.shape)
    class LinearModel(torch.nn.Module):
        def __init__(self):
            super(LinearModel, self).__init__()
            self.linear = torch.nn.Linear(1, 1)

        def forward(self, x):
            y_pred = self.linear(x)
            return y_pred


    model = LinearModel()
    criterion = torch.nn.MSELoss(size_average=False)
    optimizer = torch.optim.SGD(model.parameters(), lr=0.01)

    for epoch in range(100):
        y_pred = model(x_data)
        loss = criterion(y_pred, y_data)

        optimizer.zero_grad()
        loss.backward()
        optimizer.step()
    model.eval()
    # torch.save(model, "model-lenet-save.pt")

    print('w = ', model.linear.weight.item())
    print('b = ', model.linear.bias.item())
    x_test = torch.Tensor([[4.0]])
    y_test = model(x_test)
    traced_script_module = torch.jit.trace(model, x_test, strict=False)
    traced_script_module.save("model-lenet-export.pt")
    model=torch.load("model-lenet-export.pt")
    print('y_pred = ', y_test.data)

def main():


    args, cfg = parse_args()

    torch.backends.cudnn.deterministic = True
    checkpoint_path = utils.find_checkpoint(cfg.INTERACTIVE_MODELS_PATH, args.checkpoint)
    model = utils.load_is_model(checkpoint_path, args.device, cpu_dist_maps=True, norm_radius=args.norm_radius)
    device="cpu"
    device = torch.device("cuda:0" )

    # device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    # dummy_input1 = torch.randn(1, 3, 28, 28).to(device)
    # dummy_input2 = torch.randn(1,  28, 28).to(device)
    # input_names = ["images","points"]
    # output_names = ["heatmap"]
    # torch.onnx.export(model, (dummy_input1,dummy_input2), "fps.onnx", verbose=False, input_names=input_names,
    #                     output_names=output_names)
    # exit()

    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    model = model.to(device)
x=torch.ones(1, 3, 28, 28).to(device)
y=torch.ones(1, 28, 28).to(device)
# input1_new = x1.unsqueeze(1).expand(1, 3, 2, 2)
# inputs = torch.cat([x0, input1_new])

# x=torch.ones(2, 3, 28, 28).to(device)

    print(model.forward(x,y)[0].shape)
    torch.save(model, "model-fps-save.pt")
    # traced_script_module = torch.jit.trace(model, x, strict=True )
    traced_script_module = torch.jit.trace(model, (x,y), strict=True)
    traced_script_module.save("/home/xky/inference-serving/convert/fbrs_interactive_segmentation/cifarnet-slim/infer/models/infer_model/1/model.pt")
    exit()
    root = tk.Tk()
    root.minsize(960, 480)
    app = InteractiveDemoApp(root, args, model)
    root.deiconify()
    app.mainloop()


def parse_args():
    parser = argparse.ArgumentParser()

    parser.add_argument('--checkpoint', type=str, required=True,
                        help='The path to the checkpoint. '
                             'This can be a relative path (relative to cfg.INTERACTIVE_MODELS_PATH) '
                             'or an absolute path. The file extension can be omitted.')

    parser.add_argument('--gpu', type=int, default=0,
                        help='Id of GPU to use.')

    parser.add_argument('--cpu', action='store_true', default=False,
                        help='Use only CPU for inference.')

    parser.add_argument('--limit-longest-size', type=int, default=800,
                        help='If the largest side of an image exceeds this value, '
                             'it is resized so that its largest side is equal to this value.')

    parser.add_argument('--norm-radius', type=int, default=260)

    parser.add_argument('--cfg', type=str, default="config.yml",
                        help='The path to the config file.')

    args = parser.parse_args()
    if args.cpu:
        args.device =torch.device('cpu')
    else:
        args.device = torch.device(f'cuda:{args.gpu}')
    cfg = exp.load_config_file(args.cfg, return_edict=True)

    return args, cfg


if __name__ == '__main__':
    main()
