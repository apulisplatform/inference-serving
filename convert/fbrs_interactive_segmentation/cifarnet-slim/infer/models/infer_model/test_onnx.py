import time

import tritonclient.http as httpclient
from PIL import Image
from tritonclient.utils import *


def transform(image):
    image = np.asarray(image).astype(np.float32)
    image=np.reshape(image,(28,28,3))
    image = np.transpose(image, [2, 0, 1])
    image = image / 255
    image = np.expand_dims(image, 0)
    return image


check_img = './009_3_NG62_B_2_1936.5_3692.5_check.bmp'
model_img = './009_3_NG62_B_2_1936.5_3692.5_model.bmp'
onnx_model_path = "./efficientnet-b2_stage2_2.onnx"

# compute ONNX Runtime output prediction
model = np.ones((1,1),dtype=np.float32)
check = np.ones((2,3,28,28),dtype=np.float32)

with httpclient.InferenceServerClient("localhost:8000") as client:
    inputs = [
        httpclient.InferInput("inputs__0", check.shape,
                              np_to_triton_dtype(check.dtype))
    ]
    inputs[0].set_data_from_numpy(check, binary_data=True)
   
    result = client.infer("infer_model", inputs)
    print(result.as_numpy("outputs__0"))
    start = time.time()
    # for i in range(1000):
    #     result = client.infer("infer_model", inputs)
    #     output0 = result.as_numpy('class')
    #     # print(output0)
    #     end = time.time()
    # trt
    # 0.44251132011413574
    # 0.016350746154785156
    # 110
    # [[0.9074196]]

    # 1.320678472518921
    # 0.018003463745117188
    # 93
    # [[0.9074202]]
    print(1000 / (end - start))
