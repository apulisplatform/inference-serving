import time
import cv2
import numpy as np
from tritonclient.utils import *
import tritonclient.http as httpclient
import time

import cv2
import numpy as np
import tritonclient.http as httpclient
from tritonclient.utils import *


def transform(image):
    resize_img = cv2.resize(image, (224, 224), interpolation=cv2.INTER_CUBIC)
    type_img = resize_img.astype("float32").transpose(2, 0, 1)[np.newaxis]  # (1, 3, h, w)
    print(type_img.shape)
    type_img = type_img / 255
    return type_img

check_img = './009_3_NG62_B_2_1936.5_3692.5_check.bmp'

# compute ONNX Runtime output prediction
check = transform(cv2.imread(check_img))


with httpclient.InferenceServerClient("localhost:8000") as client:
    start = time.time()
    inputs = [
        httpclient.InferInput("gpu_0/data_0", check.shape,
                              np_to_triton_dtype(check.dtype)),
    ]
    #
    inputs[0].set_data_from_numpy(check)
    result = client.infer("tensorflow_model", inputs)
    output0 = result.as_numpy('gpu_0/softmax_1')
    print(output0)
