import json
import os
import time
import cv2
import numpy as np
from PIL import Image
from tritonclient.utils import *
import tritonclient.http as httpclient


def transform(image):
    resize_img = cv2.resize(image, (224, 224), interpolation=cv2.INTER_CUBIC)
    type_img = resize_img.astype("float32").transpose(2, 0, 1)[np.newaxis]  # (1, 3, h, w)
    print(type_img.shape)
    type_img = type_img / 255
    return type_img

check_img = './009_3_NG62_B_2_1936.5_3692.5_check.bmp'
model_img = './009_3_NG62_B_2_1936.5_3692.5_model.bmp'
onnx_model_path = "./efficientnet-b2_stage2_2.onnx"

# compute ONNX Runtime output prediction
check = transform(cv2.imread(check_img))
model = transform(cv2.imread(check_img))


with httpclient.InferenceServerClient("localhost:8000") as client:
    start = time.time()
    inputs = [
        httpclient.InferInput("gpu_0/data_0", check.shape,
                              np_to_triton_dtype(check.dtype)),
        # httpclient.InferInput("model", model.shape,
        #                       np_to_triton_dtype(check.dtype))
    ]
    #
    inputs[0].set_data_from_numpy(check)
    # inputs[1].set_data_from_numpy(model)
    # for i in range(1000):
    result = client.infer("tensorflow_model", inputs)
    output0 = result.as_numpy('class')
    print(output0)
    end=time.time()
    print(end-start)
    print(1000/(end-start))


