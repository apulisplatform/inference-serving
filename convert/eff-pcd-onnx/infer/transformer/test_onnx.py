import json
import os
import time

from PIL import Image
from tritonclient.utils import *
import tritonclient.http as httpclient


def transform(image):
    image = np.asarray(image).astype(np.float32)
    image = np.transpose(image, [2, 0, 1])
    image = image / 255
    image = np.expand_dims(image, 0)
    return image

check_img = './009_3_NG62_B_2_1936.5_3692.5_check.bmp'
model_img = './009_3_NG62_B_2_1936.5_3692.5_model.bmp'
onnx_model_path = "./efficientnet-b2_stage2_2.onnx"

# compute ONNX Runtime output prediction
check = transform(Image.open(check_img).convert("RGB"))
model = transform(Image.open(model_img).convert("RGB"))


with httpclient.InferenceServerClient("192.168.3.126:8000") as client:
    start = time.time()
    inputs = [
        httpclient.InferInput("check", check.shape,
                              np_to_triton_dtype(check.dtype)),
        httpclient.InferInput("model", model.shape,
                              np_to_triton_dtype(check.dtype))
    ]
    inputs[0].set_data_from_numpy(check)
    inputs[1].set_data_from_numpy(model)
    result = client.infer("tensorflow_model", inputs)
    output0 = result.as_numpy('class')
    for i in range(1000):
        result = client.infer("tensorflow_model", inputs)
        # output0 = result.as_numpy('class')
    print(output0)
    end=time.time()
    print(end-start)
    print(100/(end-start))
