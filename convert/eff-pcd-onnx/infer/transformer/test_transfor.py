import base64

import requests

check_img = '_8_NG198_A_1_117.857_3917.1_check.bmp'
model_img = '_8_NG198_A_1_117.857_3917.1_model.bmp'

with open(check_img, 'rb') as f:
    with open(model_img, 'rb') as f1:
        inputs = [{"data": base64.b64encode(f.read()).decode()}, {"data": base64.b64encode(f1.read()).decode()}]

res = requests.post("http://127.0.0.1:8080/v2/models/tensorflow_model/infer", json={"inputs": inputs}).text
print(res)
