import base64
import logging
from typing import Dict

import cv2
import kfserving
import numpy as np
import tritonclient.http as httpclient
from tritonclient.utils import np_to_triton_dtype

from .frame.model_parser import ModelParser


class TensorFlowModel(kfserving.KFModel):
    def __init__(self, name: str, predictor_host: str):
        super().__init__(name)
        self.load()
        self.triton_client = None
        self.model_parser = None
        self.predictor_host = predictor_host

    def preprocess(self, inputs: Dict) -> Dict:
        # check
        # model
        for input in inputs["inputs"]:
            images_data = input["data"]
            images_data = base64.b64decode(images_data)
            images_data = np.asarray(bytearray(images_data), dtype="uint8")
            image = cv2.imdecode(images_data, cv2.IMREAD_COLOR)
            image = cv2.resize(image, (200, 200))
            logging.error(image.shape)

            image = np.asarray(image).astype(np.float32)
            image = np.transpose(image, [2, 0, 1])
            image = image / 255
            image = np.expand_dims(image, 0)

            input["data"] = image
        logging.error(inputs)

        return inputs

    def predict(self, inputs: Dict) -> Dict:
        request_id = ""
        model_parser = self.get_model_parser()
        logging.error(self.get_config())
        logging.error(self.get_model_metadata())
        check = inputs["inputs"][0]["data"]
        model = inputs["inputs"][1]["data"]

        inputs = [
            httpclient.InferInput("check", check.shape,
                                  np_to_triton_dtype(check.dtype)),
            httpclient.InferInput("model", model.shape,
                                  np_to_triton_dtype(check.dtype))
        ]
        inputs[0].set_data_from_numpy(check)
        inputs[1].set_data_from_numpy(model)

        outputs = []
        for idx in range(model_parser.output_tensor_size()):
            output = httpclient.InferRequestedOutput(model_parser.output_name(idx))
            outputs.append(output)
        logging.error("begin infer")
        result = self.triton_client.infer(model_parser.model_name(), inputs, outputs=outputs, request_id=request_id)
        logging.error("after infer")
        return result

    def postprocess(self, result: Dict) -> Dict:
        postprocess_result = result.get_response()
        res_class = result.as_numpy("class")
        new_output = []
        new_output.append({'name': 'scores', 'datatype': str(res_class.dtype),
                           'shape': list(res_class.shape),
                           "data": res_class.tolist()})
        postprocess_result['outputs'] = new_output
        return postprocess_result

    def init_client(self):
        if not self.triton_client:
            self.triton_client = httpclient.InferenceServerClient(
                url=self.predictor_host, verbose=False)

    def get_model_parser(self):
        if self.model_parser is None:
            model_metadata = self.get_model_metadata()
            logging.error(model_metadata)
            model_config = self.get_config()
            self.model_parser = ModelParser(model_metadata, model_config)
        return self.model_parser

    def get_config(self):
        self.init_client()
        return self.triton_client.get_model_config(model_name=self.name)

    def get_model_metadata(self):
        self.init_client()
        return self.triton_client.get_model_metadata(model_name=self.name)
