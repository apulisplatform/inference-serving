import base64
import json
import logging

from StreamManagerApi import StreamManagerApi, MxDataInput


def run(inputs_arr, stream_manager,stream_name):
    '''
    :param
        inputs_arr [{"data":"base64"}]
    :return:
        result_list list
        time_used   string
    '''
    result_list = []
    for input in inputs_arr:
        image_content = input["data"]
        image_bytes = base64.b64decode(image_content)
        input_data = MxDataInput()
        input_data.data = image_bytes
        unique_id = stream_manager.SendDataWithUniqueId(
            stream_name, 0, input_data)
        if unique_id < 0:
            return [], 0
        raw_output = stream_manager.GetResultWithUniqueId(
            stream_name, unique_id, 30000)
        if raw_output.errorCode != 0:
            logging.error('Failed getting result with error code [{}], and message [{}]'.format(
                raw_output.errorCode, raw_output.data.decode()))
            return [], 0
        output = raw_output.data.decode()
        output = json.loads(output)
        result_list.append(output)
    return result_list


if __name__ == '__main__':
    import os
    stream_manager = StreamManagerApi()
    ret = stream_manager.InitManager()
    pipeline_config_file = "centerface.pipeline"
    with open(pipeline_config_file, "rb")as pipeline_file:
        pipeline_bytes = pipeline_file.read()
    ret = stream_manager.CreateMultipleStreams(pipeline_bytes)
    with open("test.png", 'rb') as f:
        result_list = run([{"data": base64.b64encode(f.read()).decode()}], stream_manager)
        print(result_list)
