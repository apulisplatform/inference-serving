import os
from typing import Dict

os.system(" pip install " + "sentencepiece -i https://mirrors.aliyun.com/pypi/simple")
import sentencepiece as spm

import six
import kfserving
import numpy as np
import tritonclient.http as httpclient

current_relative_path = lambda x: os.path.abspath(os.path.join(os.path.dirname(os.path.abspath(__file__)), x))

source_model = os.environ.get("SOURCE", "source.model")
target_model = os.environ.get("TARGET", "target.model")

sp_source = spm.SentencePieceProcessor(model_file=current_relative_path(source_model))
sp_target = spm.SentencePieceProcessor(model_file=current_relative_path(target_model))


def convert_to_unicode(text):
    """Converts `text` to Unicode (if it's not already), assuming utf-8 input."""
    if six.PY3:
        if isinstance(text, str):
            return text
        elif isinstance(text, bytes):
            return text.decode("utf-8", "ignore")
        else:
            raise ValueError("Unsupported string type: %s" % (type(text)))
    elif six.PY2:
        if isinstance(text, str):
            return text.decode("utf-8", "ignore")
        elif isinstance(text, unicode):
            return text
        else:
            raise ValueError("Unsupported string type: %s" % (type(text)))
    else:
        raise ValueError("Not running on Python2 or Python 3?")


def pad_batch(batch_tokens):
    """Pads a batch of tokens."""
    lengths = [len(tokens) for tokens in batch_tokens]
    max_length = max(lengths)
    for tokens, length in zip(batch_tokens, lengths):
        if max_length > length:
            tokens += ["</s>"] * (max_length - length)
    batch_tokens = [[j.encode('utf-8') for j in i] for i in batch_tokens]
    return batch_tokens, lengths, max_length


class InferModel(kfserving.KFModel):
    def __init__(self, name: str, predictor_host: str):
        super().__init__(name)
        self.load()
        self.triton_client = None
        self.model_parser = None
        self.predictor_host = predictor_host

    def to_numpy(self, tensor):
        return tensor.detach().cpu().numpy() if tensor.requires_grad else tensor.cpu().numpy()

    def preprocess(self, inputs: Dict) -> Dict:
        model_parser = self.get_model_metadata()
        for input in inputs["inputs"]:
            text = sp_source.EncodeAsPieces(convert_to_unicode(input["data"]))
            input["data"] = text + ["</s>"]
        return inputs

    def predict(self, inputs: Dict) -> Dict:
        texts = []
        for input in inputs["inputs"]:
            texts.append(input["data"])

        print("texts:", texts)
        batch_tokens, lengths, max_length = pad_batch(texts)
        np_bytes_data = np.array(batch_tokens, dtype=np.bytes_)
        lengths = np.array(lengths, dtype=np.int32)
        request_id = ""
        model_parser = self.get_model_parser()
        print(self.get_config())
        print(self.get_model_metadata())

        inputs = []
        inputs.append(httpclient.InferInput("tokens", np_bytes_data.shape, "BYTES"))
        inputs.append(httpclient.InferInput("length", lengths.shape, "INT32"))
        inputs[0].set_data_from_numpy(np_bytes_data, binary_data=False)
        inputs[1].set_data_from_numpy(lengths, binary_data=True)

        outputs = []
        outputs.append(httpclient.InferRequestedOutput("tokens", binary_data=False))

        print("begin infer")
        result = self.triton_client.infer(model_parser.model_name(), inputs, outputs=outputs, request_id=request_id)
        print("after infer")
        return result

    def postprocess(self, result: Dict) -> Dict:
        postprocess_result = result.get_response()
        outputs = []
        print(result.get_response())

        tokens = result.as_numpy('tokens')
        tokens = tokens.tolist()

        post_tokens = []
        for t in tokens[0]:
            post_tokens.append(convert_to_unicode(sp_target.DecodePieces(t)))

        outputs.append({'name': "text", 'datatype': "string",
                        'shape': len(post_tokens),
                        "data": post_tokens})

        postprocess_result['outputs'] = outputs
        return postprocess_result

    def init_client(self):
        if not self.triton_client:
            self.triton_client = httpclient.InferenceServerClient(
                url=self.predictor_host, verbose=False)

    def get_model_parser(self):
        if self.model_parser == None:
            model_metadata = self.get_model_metadata()
            model_config = self.get_config()
            self.model_parser = ModelParser(model_metadata, model_config)
        return self.model_parser

    def get_config(self):
        self.init_client()
        return self.triton_client.get_model_config(model_name=self.name)

    def get_model_metadata(self):
        self.init_client()
        return self.triton_client.get_model_metadata(model_name=self.name)


class ModelParser:
    def __init__(self, model_metadata, model_config):
        self.model_metadata = model_metadata
        self.model_config = model_config

    def input_name(self, idx):
        return self.model_config['input'][idx]['name']

    def input_dtype(self, idx):
        return self.model_metadata['inputs'][idx]['datatype']

    def max_batch_size(self):
        return self.model_config['max_batch_size']

    def input_tensor_size(self):
        return len(self.model_config['input'])

    def output_tensor_size(self):
        return len(self.model_config['output'])

    def model_name(self):
        return self.model_metadata["name"]

    def output_name(self, idx):
        outputs = self.model_metadata["outputs"]

        if (idx >= len(outputs)):
            raise Exception("outputs has {} elems,get {}'th error".format(len(outputs), idx))
        return outputs[idx]["name"]

    def parse(self):
        inputs = self.model_metadata['inputs']
        outputs = self.model_metadata['outputs']

        print(self.input_shape(0))

        if len(inputs) != 1:
            raise Exception("expecting 1 input, got {}".format(len(inputs)))
        if len(outputs) != 1:
            raise Exception("expecting 1 output, got {}".format(len(outputs)))
