import base64
from typing import Dict

import cv2
import kfserving
import numpy as np
import tritonclient.http as httpclient
from PIL import Image

from .frame.model_parser import ModelParser


class TensorFlowModel(kfserving.KFModel):
    def __init__(self, name: str, predictor_host: str):
        super().__init__(name)
        self.load()
        self.triton_client = None
        self.model_parser = None
        self.predictor_host = predictor_host

    def preprocess(self, inputs: Dict) -> Dict:
        for input in inputs["inputs"]:
            images_data = input["data"]
            images_data = base64.b64decode(images_data)
            images_data = np.asarray(bytearray(images_data), dtype="uint8")
            images = cv2.imdecode(images_data, cv2.IMREAD_COLOR)
            images = Image.fromarray(images)
            images = images.resize((512, 512), Image.BILINEAR)
            images = np.array(images)
            # images = images[np.newaxis,:,:,:]
            input["data"] = images
            print("images shape", images.shape, type(images))
        return inputs

    def predict(self, inputs: Dict) -> Dict:
        tensors = []
        for input in inputs["inputs"]:
            tensors.append(input["data"])

        tensors = np.stack(tensors, axis=0)
        request_id = ""
        model_parser = self.get_model_parser()
        print(self.get_config())
        print(self.get_model_metadata())

        inputs = []
        print("input shape", tensors.shape)
        input = httpclient.InferInput(model_parser.input_name(0), tensors.shape, model_parser.input_dtype(0))
        input.set_data_from_numpy(tensors)
        inputs.append(input)

        outputs = []
        for idx in range(model_parser.output_tensor_size()):
            output = httpclient.InferRequestedOutput(model_parser.output_name(idx))
            outputs.append(output)

        print("begin infer")
        result = self.triton_client.infer(model_parser.model_name(), inputs, outputs=outputs, request_id=request_id)
        print("after infer")
        return result

    def postprocess(self, result: Dict) -> Dict:
        postprocess_result = result.get_response()
        print(result.get_response())

        outputs = postprocess_result["outputs"]

        detection_anchor_indices = result.as_numpy("detection_anchor_indices")
        detection_boxes = result.as_numpy("detection_boxes")
        detection_classes = result.as_numpy("detection_classes")
        detection_multiclass_scores = result.as_numpy("detection_multiclass_scores")
        detection_scores = result.as_numpy("detection_scores")

        print("detection_scores", detection_scores)

        min_score_thresh = 0.5
        scores_mask = detection_scores[0, :] > 0
        new_detection_boxes = detection_boxes[:, scores_mask]
        new_detection_scores = detection_scores[:, scores_mask]
        new_detection_classes = detection_classes[:, scores_mask]

        new_output = []
        new_output.append({'name': 'detection_scores', 'datatype': 'BYTES', 'shape': new_detection_scores.shape,
                           "data": new_detection_scores.tolist()})
        new_output.append({'name': 'detection_boxes', 'datatype': 'BYTES', 'shape': new_detection_boxes.shape,
                           "data": new_detection_boxes.tolist()})
        new_output.append({'name': 'detection_classes', 'datatype': 'BYTES', 'shape': new_detection_classes.shape,
                           "data": new_detection_classes.tolist()})

        postprocess_result['outputs'] = new_output
        return postprocess_result

    def init_client(self):
        if not self.triton_client:
            self.triton_client = httpclient.InferenceServerClient(
                url=self.predictor_host, verbose=False)

    def get_model_parser(self):
        if self.model_parser == None:
            model_metadata = self.get_model_metadata()
            model_config = self.get_config()
            self.model_parser = ModelParser(model_metadata, model_config)
        return self.model_parser

    def get_config(self):
        self.init_client()
        return self.triton_client.get_model_config(model_name=self.name)

    def get_model_metadata(self):
        self.init_client()
        return self.triton_client.get_model_metadata(model_name=self.name)
