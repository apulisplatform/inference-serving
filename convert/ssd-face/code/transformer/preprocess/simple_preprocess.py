import numpy as np
import cv2
from PIL import Image
from tritonclient.utils import triton_to_np_dtype
import tritonclient.grpc.model_config_pb2 as mc

class SimplePreprocess:
    def __init__(self,img):
        self.img = img

    def process(self,format, dtype, c, h, w, scaling):
        """
        Pre-process an image to meet the size, type and format
        requirements specified by the parameters.
        """
        # np.set_printoptions(threshold='nan')
        self.img = Image.fromarray(self.img)
        if c == 1:
            sample_img = self.img.convert('L')
        else:
            sample_img = self.img.convert('RGB')
        resized_img = sample_img.resize((w, h), Image.BILINEAR)
        resized = np.array(resized_img)
        if resized.ndim == 2:
            resized = resized[:, :, np.newaxis]

        npdtype = triton_to_np_dtype(dtype)
        typed = resized.astype(npdtype)

        if scaling == 'INCEPTION':
            scaled = (typed / 127.5) - 1
        elif scaling == 'VGG':
            if c == 1:
                scaled = typed - np.asarray((128,), dtype=npdtype)
            else:
                scaled = typed - np.asarray((123, 117, 104), dtype=npdtype)
        else:
                scaled = typed

        # Swap to CHW if necessary
        if format == mc.ModelInput.FORMAT_NCHW:
            ordered = np.transpose(scaled, (2, 0, 1))
        else:
            ordered = scaled
        # Channels are in RGB order. Currently model configuration data
        # doesn't provide any information as to other channel orderings
        # (like BGR) so we just assume RGB.
        return ordered
