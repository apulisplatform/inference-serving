############################################################################
# 2019 - present Contributed by Apulis Technology (Shenzhen) Co. LTD
#
# This program and the accompanying materials are made available under the
# terms of the MIT License, which is available at
# https://www.opensource.org/licenses/MIT
#
# See the NOTICE file distributed with this work for additional
# information regarding copyright ownership.
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# SPDX-License-Identifier: MIT
############################################################################

import kfserving
import argparse
from .infer_model import InferModel

parser = argparse.ArgumentParser(parents=[kfserving.kfserver.parser])
parser.add_argument('--predictor_host', help='The URL for the model predict function')

args, _ = parser.parse_known_args()

if __name__ == "__main__":
    infer_model = InferModel("infer_model",predictor_host=args.predictor_host)
    kfserver = kfserving.KFServer()
    kfserver.start(models=[infer_model])
