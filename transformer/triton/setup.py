############################################################################
# 2019 - present Contributed by Apulis Technology (Shenzhen) Co. LTD
#
# This program and the accompanying materials are made available under the
# terms of the MIT License, which is available at
# https://www.opensource.org/licenses/MIT
#
# See the NOTICE file distributed with this work for additional
# information regarding copyright ownership.
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# SPDX-License-Identifier: MIT
############################################################################

from setuptools import setup, find_packages


setup(
    name='transformer',
    version='0.1.0',
    author_email='dsun20@bloomberg.net',
    description='Transformer',
    python_requires='>=3.6',
    packages=find_packages("transformer"),
    install_requires=[
        "kfserving>=0.4.0",
        "tensorflow==1.15.4",
        "opencv-python",
        "pillow"
    ],
)
