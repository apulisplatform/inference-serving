############################################################################
# 2019 - present Contributed by Apulis Technology (Shenzhen) Co. LTD
#
# This program and the accompanying materials are made available under the
# terms of the MIT License, which is available at
# https://www.opensource.org/licenses/MIT
#
# See the NOTICE file distributed with this work for additional
# information regarding copyright ownership.
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# SPDX-License-Identifier: MIT
############################################################################

import base64

import requests

with open("test1.jpg", 'rb') as f:
    inputs = [{"data": base64.b64encode(f.read()).decode()}]

res = requests.post("http://127.0.0.1:8080/v2/models/tensorflow_model/infer", json={"inputs": inputs}).text
print(res)
res = requests.post("http://127.0.0.1:8080/v2/models/triton_model/infer", json={"inputs": inputs}).text
print(res)
