package main

import (
        "bufio"
        "bytes"
        "encoding/json"
        "fmt"
        "io"
        "net/http"
        "io/ioutil"
        "os"
)

func main(){
        imageObj,err := os.Open("test1.jpg")
        if err != nil {
                fmt.Println("open file error",err)
                return
        }
        defer imageObj.Close()

        reader := bufio.NewReader(imageObj)

        buf := make([]byte,1024)
        chunks := make([]byte,0)

        for {
                n,err := reader.Read(buf)
                if err != nil && err != io.EOF{
                        panic(err)
                }
                if err == io.EOF || n == 0{
                                break;
                }
                chunks = append(chunks,buf[:n]...)
        }

        data := make(map[string]interface{})
        input := make(map[string]interface{})
        input["data"] = chunks
        data["inputs"] = [...]map[string]interface{}{input}

        bytesData , _ := json.Marshal(data)
        resp, err := http.Post("http://127.0.0.1:8080/v2/models/tensorflow_model/infer","application/json",bytes.NewReader(bytesData))
        fmt.Println(resp,err)
        body, err := ioutil.ReadAll(resp.Body)
        fmt.Println(resp.Header,string(body),err)
}
