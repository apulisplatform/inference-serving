# kfserving-transformer
## 连接服务器
```
ssh root@192.168.3.126
password kQ1sDC4mDZpojDo4
cd /home/xky/inference-serving/kfserving_transformer/triton

```
## 编译transformer镜像
```
docker build -t  harbor.apulis.cn:8443/algorithm/apulistech/kfserving-transformer/trition_transformer .
#或者直接拉取镜像
docker pull harbor.apulis.cn:8443/algorithm/apulistech/kfserving-transformer/trition_transformer
```

## savedmodel格式转换为Triton格式

## 启动Triton推理镜像
```
ssh root@192.168.3.126
password kQ1sDC4mDZpojDo4

#测试
#infer_model
export model_dir=/home/xky/inference-serving/convert/cifarnet-slim/infer/models
docker run  -it --gpus=1 --rm  -p8000:8000 -p8001:8001 -p8002:8002 -v ${model_dir}:/models nvcr.io/nvidia/tritonserver:20.10-py3 ./bin/tritonserver --model-repository=/models --strict-model-config=true
```

## 启动transformer镜像并挂载前后处理python脚本文件
```

# cifarnet
docker run --rm -v /home/xky/inference-serving/convert/cifarnet-slim/code/transformer:/Work/transformer  -p8080:8080 harbor.apulis.cn:8443/algorithm/apulistech/kfserving-transformer/trition_transformer --predictor_host 192.168.3.126:8000
```


## 测试脚本
```
cd /home/xky/inference-serving/kfserving_transformer/triton/test
export PATH=$PATH:/usr/local/go/bin
 (需要手动改下ip)
go run main.go
```

#### 返回结果
```
root@aistudio-base-gpu02:/home/xky/inference-serving/kfserving_transformer/triton/test# go run main.go
&{200 OK 200 HTTP/1.1 1 1 map[Content-Length:[11073] Content-Type:[application/json; charset=UTF-8] Date:[Mon, 09 Aug 2021 07:17:46 GMT] Server:[TornadoServer/6.1]] 0xc000206080 11073 [] false false map[] 0xc0001f4000 <nil>} <nil>
map[Content-Length:[11073] Content-Type:[application/json; charset=UTF-8] Date:[Mon, 09 Aug 2021 07:17:46 GMT] Server:[TornadoServer/6.1]] {"id": "0", "model_name": "SSD_tensorflow_gpu_hardhat", "model_version": "1", "outputs": [{"name": "detection_scores", "datatype": "BYTES", "shape": [1, 100], "data": [[0.7849214673042297, 0.6069959998130798, 0.5544037818908691, 0.4993760585784912, 0.46884050965309143, 0.43045464158058167, 0.30454862117767334, 0.3003750443458557,
```

#### Triton环境安装
```
python3 -m pip install  pycuda tensorrt tqdm  tensorrt -i  https://mirrors.ustc.edu.cn/pypi/web/simple    --global-option=build_ext --global-option="-I/usr/local/cuda-10.2/targets/x86_64-linux/include/" --global-option="-L/usr/local/cuda-10.2/targets/x86_64-linux/lib/" 

```