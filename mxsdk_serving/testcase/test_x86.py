# -- coding=utf-8 --
############################################################################
# 2019 - present Contributed by Apulis Technology (Shenzhen) Co. LTD
#
# This program and the accompanying materials are made available under the
# terms of the MIT License, which is available at
# https://www.opensource.org/licenses/MIT
#
# See the NOTICE file distributed with this work for additional
# information regarding copyright ownership.
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# SPDX-License-Identifier: MIT
############################################################################

import base64
import json
import logging as log

import requests

# import cv2

log.basicConfig(level=log.INFO)
7777
SERVER_URL = 'http://localhost:8888/infer'


def nlp_req():
    tensors = []
    tensors.append(
        [[[101, 2002, 2056, 1996, 9440, 2121, 7903, 2063, 11345, 2449, 2987, 1005, 1056, 4906, 1996, 2194, 1005, 1055,
           2146, 1011, 2744, 3930, 5656, 1012, 102, 1000, 1996, 9440, 2121, 7903, 2063, 11345, 2449, 2515, 2025, 4906,
           2256, 2146, 1011, 2744, 3930, 5656, 1012, 102, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
           0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
           0,
           0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]],
         [[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
           1,
           1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
           0,
           0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
           0,
           0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]],
         [[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
           1,
           1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
           0,
           0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
           0,
           0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]]])

    res = requests.post(SERVER_URL, json={"inputs": tensors})
    print(res.text)
    json.dump(res.json(), open("res.json", "w"))
    print(res.json())


def ssd_req():
    inputs = []
    input_file = "test.jpg"
    with open(input_file, 'rb') as f:
        inputs.append({"data": base64.b64encode(f.read()).decode()})
    res = requests.post(SERVER_URL, json={"inputs": inputs}).json()
    # image = cv2.imread(input_file)
    # for rect in res['outputs'][0]["MxpiObject"]:
    #     confidence = rect["classVec"][0]["confidence"]
    #     if confidence > 0.35:
    #         draw = cv2.rectangle(image, (int(rect["x0"]), int(rect["y0"])), (int(rect["x1"]), int(rect["y1"])),
    #                              (0, 255, 0), 2)
    #         className = rect["classVec"][0]["className"]
    #         confidence = rect["classVec"][0]["confidence"]
    #         cv2.putText(image, str(className), (int(rect["x0"]), int(rect["y0"])+20), cv2.FONT_HERSHEY_SIMPLEX, 1,
    #                     (255, 255, 0), 2)
    #         cv2.putText(image, str(confidence), (int(rect["x1"])-100, int(rect["y0"])+20), cv2.FONT_HERSHEY_SIMPLEX, 1,
    #                     (255, 255, 0), 2)
    #         print(className, confidence)
    # cv2.imwrite("result.jpg", draw)
    print(res)


if __name__ == '__main__':
    ssd_req()
    # import multiprocessing
    # for i in range(1000):
    #     p = multiprocessing.Process(target=ssd_req, args=())
    #     p.start()

#   nlp_req()
