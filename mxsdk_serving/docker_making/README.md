# 运行说明

1. 构建和启动容器
   
   执行脚本
   ```shell
   bash 01_docker_build_run.sh 210630 8896 4 /ascend/715/mx_ai_server_docker/210630
   ```

2. 查看容器
   ```
   docker ps | grep infer
   ```

3. 查看日志
   ```
   docker logs [容器名称]
   ```

   ```
   docker exec [容器名称] tail -f /home/ai_inference_atlas_sdk/logs/ai_server.log
   ```

3. 运行自动化测试脚本
   用unittest进行用例测试: 修改 SERVER_URL, TASK_NAME, model_pro_dic

   ```
   cd ../sample
   python -m unittest test_call_mxAoiService_x86_210626
   ```