#!/bin/bash

# get cpu arch, remove unnecessary run packages
ArchType='amd64'
if [ "$(uname -a | grep aarch64)" != "" ] ; then
    ${ArchType}='arm64'
    ls *x86* | xargs rm -f
else
    ${ArchType}='amd64'
    ls *aarch* | xargs rm -f
    ls *arm* | xargs rm -f
fi

# set version_no
if [ -n "$1" ] ; then
    Today="$1"
else
    Today=`date +%y%m%d`
fi
MainVerion='2.0.2'
VesionNo=${MainVerion}'.'${Today}
echo "version_no:${VesionNo}"

# set port_no
if [ -n "$2" ] ; then
    PortNo=$2
else
    PortNo=8895
fi

# set npu_no
if [ -n "$3" ] ; then
    NpuNo=$3
else
    NpuNo=0
fi

# set inference service parent dir
if [ -n "$4" ] ; then
    MX_INFER_SDK_DIR=$4
else
    MX_INFER_SDK_DIR=$(dirname $0)+"mxsdk_serving"
fi

# bulid
docker build -t harbor.apulis.cn:8443/algorithm/apulistech/ascend-inference-serving:1.0-${ArchType} -f Dockerfile .
export NpuNo=3
docker run   -d   \
    --device=/dev/davinci${NpuNo}  \
    --device=/dev/davinci_manager \
    --device=/dev/devmm_svm \
    --device=/dev/hisi_hdc \
    -p 8895:8888 \
    -v /usr/local/Ascend/driver:/usr/local/Ascend/driver \
    -v /usr/local/bin/npu-smi:/usr/local/bin/npu-smi \
    -v /home/xky/mxsdk_serving:/home/apflow_mxsdk_serving \
    -v /home/xky/mxsdk_serving/config/models:/home/apflow_mxsdk_serving/config/models \
    harbor.apulis.cn:8443/algorithm/apulistech/ascend-inference-serving:1.0-${ArchType}

#用于测试
#docker run -it   --rm --privileged \
#    --device=/dev/davinci${NpuNo}  \
#    --device=/dev/davinci_manager \
#    --device=/dev/devmm_svm \
#    --device=/dev/hisi_hdc \
#    -v /home/xky:/home/xky \
#    -p 8895:8888 \
#    -v /usr/local/Ascend/driver:/usr/local/Ascend/driver \
#    -v /usr/local/bin/npu-smi:/usr/local/bin/npu-smi \
#    -v /home/xky/mxsdk_serving:/home/apflow_mxsdk_serving \
#    -v /home/xky/mxsdk_serving/config/models:/home/apflow_mxsdk_serving/config/models \
#    harbor.apulis.cn:8443/algorithm/apulistech/ascend-inference-serving:1.0-${ArchType}  bash
# watch running containers
docker ps | grep infer

# 用于测试
# docker run -it \
# --restart=always \
# --device=/dev/davinci3  \
# --device=/dev/davinci_manager \
# --device=/dev/devmm_svm \
# --device=/dev/hisi_hdc \
# -p 8895:8888 \
# -v /usr/local/Ascend/driver:/usr/local/Ascend/driver \
# -v /usr/local/bin/npu-smi:/usr/local/bin/npu-smi \
# -v ${MX_INFER_SDK_DIR}/mxAOIService:/home/ai_inference_atlas_sdk \
# -v ${MX_INFER_SDK_DIR}/om_mxmanufacture:/home/ai_inference_atlas_sdk/config/models \
# harbor.apulis.cn:8443/huawei/ascend-infer/arm64:${VesionNo}  bash
