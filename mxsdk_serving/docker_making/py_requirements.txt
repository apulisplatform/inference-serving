attrs
numpy
decorator
sympy==1.4
cffi==1.12.3
pyyaml
pathlib2
psutil
scipy
xlrd==1.2.0
flask==1.1.2
opencv-python-headless
requests==2.24.0
protobuf==3.13.0
pika==1.2.0
flask-limiter==1.4
PyYaml==5.4.1
coverage==5.5
gevent==20.9.0
cython
pandas
opencv-python
pillow
pycocotools
mmcv==0.2.14
pip install bert-tensorflow==1.0.1
pip install tensorflow-cpu==1.15