#!/bin/bash

# set version_no
if [ -n "$1" ] ; then
    VesionNo=$1
else
    Today=`date +%y%m%d`
    MainVerion='2.0.2'
    VesionNo=${MainVerion}'.'${Today}
fi
echo "version_no:${VesionNo}"

BaseImgName='harbor.apulis.cn:8443/huawei/ascend-infer'
ImgName=${BaseImgName}:${VesionNo}
# create manifest
docker manifest create ${ImgName}:${VesionNo} \
    ${ImgName}/amd64:${VesionNo} \
    ${ImgName}/arm64:${VesionNo}

# annotate manifest for amd64
docker manifest annotate ${ImgName}:${VesionNo} \
    ${ImgName}/amd64:${VesionNo} \
    --os linux --arch amd64

# annotate manifest for arm64
docker manifest annotate ${ImgName}:${VesionNo} \
    ${ImgName}/arm64:${VesionNo} \
    --os linux --arch arm64

# inspect manifest
docker manifest inspect ${ImgName}:${VesionNo} 

# push manifest
docker manifest push ${ImgName}:${VesionNo} 

echo 'succeed to make manifest:'