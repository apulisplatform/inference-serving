#!/bin/bash

# set port_no
if [ -n "$1" ] ; then
    PortNo=$1
else
    PortNo=8895
fi

cid=`docker ps | grep infer | grep ${PortNo} | awk '{print $1}'`

if [ -n "${cid}" ]; then
    c_str=`docker ps --format "{{.ID}}, name: {{.Names}}, image:{{.Image}}" | grep ${cid}`
    echo "delete: ${c_str}"
    docker stop ${cid} && docker rm ${cid}
fi