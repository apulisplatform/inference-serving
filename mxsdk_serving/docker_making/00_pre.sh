#!/bin/bash

if [ "$(uname -a | grep aarch64)" != "" ] ; then 
    ls *x86* | xargs rm -f
else
    ls *aarch* | xargs rm -f
    ls *arm* | xargs rm -f
fi
old_dir=`pwd`
ls *.sh | xargs dos2unix

rm -rf ../mxAOIService
rm -rf ../om_mxmanufacture
tar xzvf Ascend-mxAOIService-2.0.2.*-linux-*.tar.gz -C ..
