#!/bin/bash

arm_image=harbor.apulis.cn:8443/algorithm/apulistech/ms-tf-pytorch-train:1.3.0-1.15.0-1.5.0-arm64
amd_image=harbor.apulis.cn:8443/algorithm/apulistech/ms-tf-pytorch-train:1.3.0-1.15.0-1.5.0-amd64
manifest_name=harbor.apulis.cn:8443/algorithm/apulistech/ms-tf-pytorch-train:1.3.0-1.15.0-1.5.0

echo $manifest_name

rm -r ~/.docker/manifests

echo "arm image :$arm_image"
echo "amd image :$amd_image"

docker manifest create $manifest_name $arm_image $amd_image --amend
docker manifest annotate $manifest_name $arm_image --arch arm64 --os linux
docker manifest annotate $manifest_name $amd_image --arch amd64 --os linux
docker manifest push $manifest_name
