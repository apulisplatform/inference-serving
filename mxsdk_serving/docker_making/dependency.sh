execpath=$(dirname $(realpath $0))
pip install cython
source $execpath/parsers.sh
# export PYTHONPATH=/home/admin/centerface2/model-gallery/models/npu/mindspore/centerface
if python -c "import nms" > /dev/null 2>&1
then
    echo "NMS module already exits, no need reinstall."
    cd $execpath
else
    echo "NMS module was not found, install it now..."
    cd $execpath/dependency/centernet/src/lib/external
    sudo python setup.py install
    sudo make
    cd $execpath/dependency/evaluate
    sudo python setup.py install
    cd $execpath
fi
