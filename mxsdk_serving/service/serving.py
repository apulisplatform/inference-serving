############################################################################
# 2019 - present Contributed by Apulis Technology (Shenzhen) Co. LTD
#
# This program and the accompanying materials are made available under the
# terms of the MIT License, which is available at
# https://www.opensource.org/licenses/MIT
#
# See the NOTICE file distributed with this work for additional
# information regarding copyright ownership.
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# SPDX-License-Identifier: MIT
############################################################################

import argparse
import json
import os
import time
from http import HTTPStatus

import numpy as np
import yaml
from flask import Flask, request, jsonify
from flask_limiter import Limiter
from gevent import pywsgi
from logger import logger as logging
# monkey.patch_all()
from model_infer import Model

LOGGING_DIR = 'logs'
USER_CONFIG = "./config/config.yaml"
cov = None
TASK_TYPE = ['cv-detection', "nlp-classification"]


class NpEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, np.integer):
            return int(obj)
        if isinstance(obj, np.floating):
            return float(obj)
        if isinstance(obj, np.ndarray):
            return obj.tolist()
        return super(NpEncoder, self).default(obj)


def create_app(model_infer):
    app = Flask(__name__, root_path=os.getcwd())
    app.config['LOG_FOLDER'] = LOGGING_DIR
    if not os.path.isdir(app.config['LOG_FOLDER']):
        os.makedirs(app.config['LOG_FOLDER'], mode=0o750)

    colloid_template_map = {}

    def _key_func():
        return request.content_type in ["application/json"]

    def _load_rate_limit():
        yaml_path = os.path.join(app.root_path, 'config', 'config.yaml')
        with open(yaml_path, encoding='utf-8') as f:
            yaml_config = yaml.safe_load(f)
            return yaml_config['rate_limit'] or '100/minute'

    rate_limit = _load_rate_limit()

    limiter = Limiter(app, key_func=_key_func, default_limits=[rate_limit])

    def _deal_with_json_data():
        request_time_start = time.time()
        json_data = request.get_json()
        inputs = json_data.get("inputs")
        print("======================================inputs======================================")
        print(inputs)
        results_list, time_list = model_infer.run_inference(inputs)
        res_dict = _deal_with_result(results_list, time_list, request_time_start)
        return jsonify(res_dict), HTTPStatus.OK

    def _deal_with_result(
            results_list,
            infer_time,
            request_time_start):
        request_time_end = time.time()
        res_dict = {"outputs": results_list,
                    "inferTime": {"all": request_time_end - request_time_start, "infer": infer_time}}

        res_dict = json.dumps(res_dict, cls=NpEncoder)
        res_dict = json.loads(res_dict)
        print("======================================result======================================")
        print(res_dict)
        return res_dict

    @app.route('/get_server_info', methods=['GET', 'POST'])
    def get_model_infos():
        return jsonify(model_infer.get_model_type_map())

    @app.route('/infer', methods=['POST'])
    # @limiter.limit(rate_limit, methods=['POST'])
    @limiter.exempt
    def infer():
        if request.content_type == "application/json":
            res_dict, status = _deal_with_json_data()
        else:
            logging.error("Unsupported content type: {}".format(request.content_type))
            return jsonify({"msg": "Unsupported content type: {}".format(request.content_type)}, HTTPStatus.BAD_REQUEST)
        return res_dict, status

    return app


def str_2_bool(input):
    if input.lower() == 'true':
        return True
    elif input.lower() == 'false':
        return False
    else:
        raise argparse.ArgumentTypeError('Unsupported value encountered.')


def main():
    parser = argparse.ArgumentParser(description='AI Inference service based on Atlas ACL framework')
    parser.add_argument("--host", "-i", default="0.0.0.0", help="The host of the server")
    parser.add_argument("--port", "-p", default=8888, type=int, help="The port of the server")
    parser.add_argument("--https", "-s", default=False, type=str_2_bool,
                        help="Enable https or http, default is https. When http is used, data is transmitted in "
                             "plaintext on the network, which may cause data leakage. Exercise caution when using http")
    parser.add_argument("--device_id", "-d", type=int, help="Device ID")
    parser.add_argument("--test", "-t", default=False, type=str_2_bool, help="code coverage")
    parser.add_argument("--upload", "-u", default=False, type=str_2_bool,
                        help="Upload image and infer result. The default value is false.")

    args = parser.parse_args()
    global HTTPS
    HTTPS = args.https
    pipeline_config_file = None
    # -infer
    #   --ssd.pipeline
    # -infer.yaml
    infer_yaml_file = os.path.join(Model.MODEL_DIR, "../", "../", "infer.yaml")
    print("infer_yaml_file", infer_yaml_file)
    logging.error(infer_yaml_file)
    if not os.path.exists(infer_yaml_file):
        print("cannot find infer_yaml_file file!")
        logging.error("cannot find infer_yaml_file file!")
        return
    # pipeline放在transformer
    for filename in os.listdir(os.path.join(Model.MODEL_DIR)):
        if filename.endswith("pipeline"):
            pipeline_config_file = os.path.join(Model.MODEL_DIR, filename)
    print("pipeline_config_file", pipeline_config_file)
    logging.error(pipeline_config_file)
    if pipeline_config_file:
        # log version info
        model_infer = Model(pipeline_config_file, infer_yaml_file, args.device_id)
        app = create_app(model_infer)
        # https or http server
        server = pywsgi.WSGIServer((args.host, args.port), app)
        server.serve_forever()
        # app.run(host='0.0.0.0', port=8888, debug=False, use_reloader=False,threaded=True)
    else:
        print("cannot find pipeline file!")
        logging.error("cannot find pipeline file!")
        return


if __name__ == '__main__':
    main()
