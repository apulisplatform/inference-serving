#!/bin/bash
############################################################################
# 2019 - present Contributed by Apulis Technology (Shenzhen) Co. LTD
#
# This program and the accompanying materials are made available under the
# terms of the MIT License, which is available at
# https://www.opensource.org/licenses/MIT
#
# See the NOTICE file distributed with this work for additional
# information regarding copyright ownership.
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# SPDX-License-Identifier: MIT
############################################################################

set -e

usage() {
  cat << EOF
Usage: ./start.sh [options]

Options:
  -h/--help         display this help and exit
  -d/--device_id    set device id
  -i/--host         listening ip address. The default value is 127.0.0.1
  -p/--port         listening port. The default value is 8888.
  -s/--https        HTTPS option. The default value is true, indicating that HTTPS is enabled. If this parameter is set to false,
                    HTTP is enabled. When HTTP is used, data is transmitted in plaintext on the network, which may cause data leakage.
                    Exercise caution when using http.
  -u/--upload       Upload image and infer result. The default value is false.
EOF
}

device_id=
https=
host=
port=
upload=

while [ "$1" != "" ]; do
    case "$1" in
        -d | --device_id )          shift
                                    device_id="$1"
                                    ;;
        -i | --host )               shift
                                    host="$1"
                                    ;;
        -p | --port )               shift
                                    port="$1"
                                    ;;
        -s | --https )              shift
                                    https="$1"
                                    ;;
        -u | --upload )             shift
                                    upload="$1"
                                    ;;
        -h | --help )               usage
                                    exit
                                    ;;
        * )                         usage
                                    exit 1
                                    ;;
    esac
    shift
done

cd "$(dirname "$0")"
CUR_PATH=$(pwd)
echo $(pwd)

if [ "${MX_SDK_HOME}" ];then
    echo "MX_SDK_HOME=${MX_SDK_HOME}"
else
    echo "MX_SDK_HOME is not set, MindX SDK is not installed."
    exit 1
fi

if [ "${INFER_LOG_DIR}" ];then
    echo "log_dir=$INFER_LOG_DIR" >>"$(realpath "${CUR_PATH}/config/logging.conf")"
else
    echo "log_dir=logs" >>"$(realpath "${CUR_PATH}/config/logging.conf")"
fi

export LD_LIBRARY_PATH=${CUR_PATH}/lib:${LD_LIBRARY_PATH}

arguments=
if [ "$device_id" ]; then
    arguments="${arguments} -d ${device_id}"
fi
if [ "$host" ]; then
    arguments="${arguments} -i ${host}"
fi
if [ "$port" ]; then
    arguments="${arguments} -p ${port}"
fi
if [ "$https" ]; then
    arguments="${arguments} -s ${https}"
fi
if [ "$upload" ]; then
    arguments="${arguments} -u ${upload}"
fi

if [ "${arguments}" ]; then
    python3.7 serving.py ${arguments}
else
    python3.7 serving.py
fi
