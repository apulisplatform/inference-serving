############################################################################
# 2019 - present Contributed by Apulis Technology (Shenzhen) Co. LTD
#
# This program and the accompanying materials are made available under the
# terms of the MIT License, which is available at
# https://www.opensource.org/licenses/MIT
#
# See the NOTICE file distributed with this work for additional
# information regarding copyright ownership.
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# SPDX-License-Identifier: MIT
############################################################################

import os
from logging.handlers import BaseRotatingHandler, RotatingFileHandler
import logging
import yaml

_config_path = 'config/config.yaml'


def load_logging_config(config_path):
    with open(config_path, encoding='utf-8') as f:
        logging_conf = yaml.safe_load(f)
        return logging_conf.get('ai_server_logging_config')


class RotatingHandlerBaseRewrite(BaseRotatingHandler):

    def _open(self):
        return os.fdopen(os.open(self.baseFilename, os.O_RDWR | os.O_CREAT, 0o600), 'a')


class RotatingLogFileHandler(RotatingFileHandler, RotatingHandlerBaseRewrite):
    def __init__(self, filename, mode='a', max_bytes=0, backup_count=0, encoding=None, delay=False):
        if max_bytes > 0:
            mode = 'a'
        RotatingHandlerBaseRewrite.__init__(self, filename, mode, encoding, delay)
        self.maxBytes = max_bytes
        self.backupCount = backup_count


def get_logger(filepath, log_formatter, max_bytes, backup_count, level=logging.INFO):
    logger = logging.getLogger("ai_server")
    handler = RotatingLogFileHandler(filepath, 'a', max_bytes, backup_count)
    handler.setFormatter(log_formatter)
    logger.addHandler(handler)
    logger.setLevel(level)
    return logger


logging_config = load_logging_config(_config_path)
default_logdir = 'logs'
if not os.path.isdir(default_logdir):
    os.makedirs(default_logdir)

_log_formatter = logging.Formatter(
    logging_config.get(
        'formatter', '%(levelname)s %(asctime)s [%(filename)s:%(lineno)d] %(message)s'),
    logging_config.get('datefmt', '%Y-%m-%d %H:%M:%S'))
logger = get_logger(logging_config.get('filepath', 'logs/ai_server.log'),
                    _log_formatter,
                    logging_config.get('max_bytes', 100 * 1024 * 1024),
                    logging_config.get('backup_count', 10))
