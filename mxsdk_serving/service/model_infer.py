############################################################################
# 2019 - present Contributed by Apulis Technology (Shenzhen) Co. LTD
#
# This program and the accompanying materials are made available under the
# terms of the MIT License, which is available at
# https://www.opensource.org/licenses/MIT
#
# See the NOTICE file distributed with this work for additional
# information regarding copyright ownership.
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# SPDX-License-Identifier: MIT
############################################################################

import json
import logging
import os
import time

import MxpiDataType_pb2 as MxpiDataType
import numpy as np
import yaml
from StreamManagerApi import StreamManagerApi, MxDataInput, InProtobufVector, MxProtobufIn, StringVector


class Model:
    SRC_PLUGIN_ID = 0
    ERROR_CODE_OK = 0
    TIME_OUT = 30000
    MODEL_DIR = "config/models/infer/transformer"
    def __init__(self, pipeline_config_file, infer_yaml_file, device_id=0):
        self._stream_manager = StreamManagerApi()
        ret = self._stream_manager.InitManager()
        if ret != Model.ERROR_CODE_OK:
            logging.error('Failed initializing StreamManager!')
            raise Exception('Failed initializing StreamManager!')
        with open(pipeline_config_file, "rb")as pipeline_file:
            pipeline_dict = json.load(pipeline_file)
            stream_name = list(pipeline_dict.keys())[0]
            infer_key = "mxpi_modelinfer0"
            if pipeline_dict[stream_name].__contains__("mxpi_modelinfer0"):
                infer_key = "mxpi_modelinfer0"
            elif pipeline_dict[stream_name].__contains__("mxpi_tensorinfer0"):
                infer_key = "mxpi_tensorinfer0"
            props = pipeline_dict[stream_name][infer_key]["props"]
            props = self.append_path(props, "modelPath")
            props = self.append_path(props, "postProcessConfigPath")
            props = self.append_path(props, "postProcessLibPath")
            props = self.append_path(props, "labelPath")
            pipeline_dict[stream_name][infer_key]["props"] = props
            pipeline_bytes = json.dumps(pipeline_dict).encode("utf8")
            self._stream_name = stream_name.encode("utf8")

        with open(infer_yaml_file, "r", encoding='utf-8')as yaml_file:
            yaml_config = yaml.safe_load(yaml_file.read())
            self._field = yaml_config.get("field")
            self._task = yaml_config.get("task")
            self._labels = yaml_config.get("labels")
            self._infer_scene = self._field + "-" + self._task
        ret = self._stream_manager.CreateMultipleStreams(pipeline_bytes)
        if ret != Model.ERROR_CODE_OK:
            logging.error('Failed creating multiple streams!')
            raise Exception('Failed creating multiple streams!')

    def __del__(self):
        self._stream_manager.DestroyAllStreams()

    def append_path(self, dic, key):
        if dic.__contains__(key):
            dic[key] = os.path.join(self.MODEL_DIR, dic[key])
        return dic

    def infer_protobuf_input(self, inputs_arr):
        time_start = time.time()
        inPluginId = 0
        result_list = []
        for inputs in inputs_arr:
            for input in inputs:
                input = np.asarray(input, dtype=np.int32)
                tensorPackageList = MxpiDataType.MxpiTensorPackageList()
                tensorPackage = tensorPackageList.tensorPackageVec.add()
                print(input.shape)
                array_bytes = input.tobytes()
                dataInput = MxDataInput()
                dataInput.data = array_bytes
                tensorVec = tensorPackage.tensorVec.add()
                tensorVec.deviceId = 0
                tensorVec.memType = 0
                for i in input.shape:
                    tensorVec.tensorShape.append(i)
                tensorVec.dataStr = dataInput.data
                tensorVec.tensorDataSize = len(array_bytes)
                key = "appsrc{}".format(inPluginId).encode('utf-8')
                protobufVec = InProtobufVector()
                protobuf = MxProtobufIn()
                protobuf.key = key
                protobuf.type = b'MxTools.MxpiTensorPackageList'
                protobuf.protobuf = tensorPackageList.SerializeToString()
                protobufVec.push_back(protobuf)
                ret = self._stream_manager.SendProtobuf(self._stream_name, inPluginId, protobufVec)
                inPluginId += 1
                if ret < 0:
                    logging.error("Failed to send data to stream.")
                    return [], 0
            keyVec = StringVector()
            keyVec.push_back(b'mxpi_tensorinfer0')
            inferResult = self._stream_manager.GetProtobuf(self._stream_name, 0, keyVec)
            if inferResult.size() == 0:
                print("inferResult is null")
                logging.error("inferResult is null")
                return [], 0
            if inferResult[0].errorCode != 0:
                logging.error("GetProtobuf error. errorCode=%d" % (
                    inferResult[0].errorCode))
                return [], 0
            preds = MxpiDataType.MxpiTensorPackageList()
            preds.ParseFromString(inferResult[0].messageBuf)
            result = []
            for tensor_vec in preds.tensorPackageVec[0].tensorVec:
                pred = np.frombuffer(tensor_vec.dataStr, dtype='<f4')
                result.append(pred.tolist())
            result_list.append(result)
        time_used = time.time() - time_start
        return result_list, time_used

    def run_inference(self, inputs_arr):
        import importlib
        time_start = time.time()
        infer_pipeline = importlib.import_module(f"config.models.infer.transformer.ascend_model")
        result_list = infer_pipeline.run(inputs_arr, self._stream_manager,self._stream_name)
        time_used = time.time() - time_start
        return result_list, time_used
