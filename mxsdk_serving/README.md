# inference-serving

Ascend MindXSDK Inference Serving

# 服务启动流程

- 挂载的模型目录树
```shell script
convert/ascend
├── code
└── infer
    ├── infer.yaml
    ├── keep
    ├── model.cfg
    ├── model.name
    ├── model.pipeline
    ├── postprocessor.so
    ├── resnet.air
    ├── ssd.pipeline
    └── tinybert.om

```

- 连接服务器

```
ssh root@192.168.3.70
```

- 拉取推理镜像

```
cd /home/xky/inference-serving/sample
docker pull harbor.apulis.cn:8443/algorithm/apulistech/ascend-inference-serving:1.0-amd64

```

- docker启动，挂载设备、SDK路径、模型路径(如果更換模型，修改model_dir)
```
#nlp分类
export model_dir=/home/xky/inference-serving/convert/ssd-ascend/
#SSD目标检测
export model_dir=/home/xky/inference-serving/convert/centerface-ascend/
export port=7777
export npu_no=0
docker run   -d  --rm  \
    --device=/dev/davinci${npu_no}  \
    --device=/dev/davinci_manager \
    --device=/dev/devmm_svm \
    --device=/dev/hisi_hdc \
    -p ${port}:8888 \
    -v /usr/local/Ascend/driver:/usr/local/Ascend/driver \
    -v /usr/local/bin/npu-smi:/usr/local/bin/npu-smi \
    -v /home/xky/mxsdk_serving:/home/apflow_mxsdk_serving \
    -v ${model_dir}:/home/apflow_mxsdk_serving/service/config/models \
    harbor.apulis.cn:8443/algorithm/apulistech/ascend-inference-serving:1.0-amd64
```
- 测试模式启动服务

```
export model_dir=/home/xky/inference-serving/convert/centerface-ascend/
docker run -it   --entrypoint /bin/bash --rm --privileged \
    --device=/dev/davinci0  \
    --device=/dev/davinci_manager \
    --device=/dev/devmm_svm \
    --device=/dev/hisi_hdc \
    -v /home/xky:/home/xky \
    -p 8895:8888 \
    -v /usr/local/Ascend/driver:/usr/local/Ascend/driver \
    -v /usr/local/bin/npu-smi:/usr/local/bin/npu-smi \
    -v /home/xky/inference-serving/mxsdk_serving:/home/apflow_mxsdk_serving \
    -v ${model_dir}:/home/apflow_mxsdk_serving/service/config/models \
    harbor.apulis.cn:8443/algorithm/apulistech/ascend-inference-serving:1.0-amd64

cd /home/apflow_mxsdk_serving/service 
python serving.py
```

- 启动python中断运行request测试请求

```
cd home/xky/mxsdk_serving/testcase/
python test_x86.py
```

- 正确返回

```
#nlp
{'inferTime': {'all': 0.046878814697265625, 'infer': 0.023370981216430664, 'request': 0.02350783348083496}, 'outputs': [[-0.13037109375, -2.09765625]]}


#cv-det
##请求 ，

with open("test.jpg", 'rb') as f:
    inputs.append({"data": base64.b64encode(f.read()).decode()})
res = requests.post(SERVER_URL, json={"inputs": inputs})

# 输入请求为json格式的base64字符串，一个base64代表一张图片。输入分辨率 300*300
request = {"inputs": [{data:"base64"},{data:"base64"}]}

## 返回
{
  "inferTime": {
    #单次请求所需的所有时间，秒为单位
    "all": 0.09995627403259277,
    #单次请求所需的推理部分时间
    "infer": 0.09941744804382324
  },
  "outputs": [
    {
    #固定输出字段名称，数组中每个元素则代表每个检测框结果
      "MxpiObject": [
        {
           #单个框信息
          "classVec": [
            {
              #输出标签ID
              "classId": 1,
              #输出标签名称
              "className": "person",
              #当前框的置信度，代表属于该类的概率，可以进行筛选，值越大代表该框越可能为人
              "confidence": 0.630859375,
              #暂为空
              "headerVec": []
            }
          ],
          #输出框左上角(x0,y0)，右下角(x1,y1)
          "x0": 370.195312,
          "x1": 526.992188,
          "y0": 112.5,
          "y1": 576
        },
        {
          "classVec": [
            {
              "classId": 1,
              "className": "person",
              "confidence": 0.588867188,
              "headerVec": []
            }
          ],
          "x0": 237.128906,
          "x1": 350.332031,
          "y0": 45.7734375,
          "y1": 277.03125
        }
      ]
    }
  ]
}
```