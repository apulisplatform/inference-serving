# inference-serving

Ascend MindXSDK Inference Serving

# 目录结构

- transformer 
    - GPU、NPU推理标准transformer前后处理代码

- mxsdk_serving
    - NPU MindX SDK推理框架
- convert
    - 多框架、多设备模型转换工具 
